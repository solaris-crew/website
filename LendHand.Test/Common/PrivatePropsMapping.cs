﻿using AutoMapper;
using FluentAssertions;
using LendHand.Contracts.Dto.Bot;
using LendHand.Telegram.Business.Mapping;
using LendHand.Telegram.Data.Entities;
using NUnit.Framework;

namespace LendHand.Test.Common
{
    public class PrivatePropsMapping
    {
        [Test]
        public void MapVolunteer()
        {
            var config = new MapperConfiguration(x => x.AddProfile(new BotProfile()));
            var mapper = config.CreateMapper();

            var entity = new Invocation
            {
                Id = 666,
                HelpTypeId = 1234,
                VolunteerId = 14,
                NeedyId = 88
            };

            var dto = mapper.Map<InvocationDto>(entity);

            dto.Should().NotBeNull();
            dto.Id.Should().Be(666);
            dto.HelpTypeId.Should().Be(1234);
            dto.VolunteerId.Should().Be(14);
            dto.NeedyId.Should().Be(88);

        }
    }
}
