﻿using AutoMapper;
using FluentAssertions;
using LendHand.Contracts.Dto.Bot;
using LendHand.Telegram.Business.Mapping;
using LendHand.Telegram.Business.Services.Interfaces;
using LendHand.Telegram.Data;
using LendHand.Telegram.Data.Entities;
using LendHand.Telegram.Data.Repository;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace LendHand.Test.InvocationService
{
    public class ServiceTest
    {
        private TelegramDbContext context;
        private IVolunteerRepository volunteerRepository;
        private IInvocationRepository invocationRepository;
        private IInvocationService service;

        [Test]
        public async Task Add()
        {
            var add = new AddInvocationDto
            {
                NeedyId = 1,
                HelpTypeId = 1
            };

            await this.service.Add(add);

            var exists = await this.context.Invocation.ToListAsync();

            exists.Count.Should().Be(1);
            exists.FirstOrDefault().Should().NotBeNull();
            exists.FirstOrDefault().NeedyId.Should().Be(1);
        }

        [OneTimeSetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<TelegramDbContext>()
                .UseInMemoryDatabase(databaseName: "inmemorydb")
                .Options;

            this.context = new TelegramDbContext(options);
            this.volunteerRepository = new VolunteerRepository(this.context);
            this.invocationRepository = new InvocationRepository(this.context);

            var config = new MapperConfiguration(x => x.AddProfile(new BotProfile()));
            this.service = new Telegram.Business.Services.InvocationService(
                this.volunteerRepository,
                invocationRepository,
                config.CreateMapper());

            this.context.Add(new Needy
            {
                Id = 1,
                Name = "Needy 1"
            });
            this.context.Add(new Volunteer
            {
                Id = 2,
                Name = "Volunteer 1"
            });
        }
    }
}
