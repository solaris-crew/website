﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using LendHand.Telegram.Data;
using Microsoft.EntityFrameworkCore;
using LendHand.Telegram.Data.Repository;
using LendHand.Telegram.Business.Services.Interfaces;
using AutoMapper;
using LendHand.Telegram.Business.Mapping;
using LendHand.Telegram.Data.Entities;
using LendHand.Contracts.Dto.Bot;

namespace LendHand.Test.VolunteerService
{
    public class VolunterrCrud
    {
        private TelegramDbContext context;
        private IVolunteerRepository repo;
        private IVolunteerService service;

        [Test]
        public void AddVolunteer()
        {
            var volunteer = new VolunteerDto
            {
                IsPoliciesAccepted = true,
                LanguageCode = "en",
                Name = "Test Member",
                TelegramChatId = 111111
            };

            var result = this.service.Add(volunteer)
                .GetAwaiter()
                .GetResult();

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result);

        }

        [OneTimeSetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<TelegramDbContext>()
                .UseInMemoryDatabase(databaseName: "inmemorydb")
                .Options;

            this.context = new TelegramDbContext(options);
            this.repo = new VolunteerRepository(this.context);

            var config = new MapperConfiguration(x => x.AddProfile(new BotProfile()));
            this.service = new LendHand.Telegram.Business.Services.VolunteerService(this.repo, config.CreateMapper());
        }

    }
}
