﻿using LendHand.Resources.Data.Entities;
using System.Threading.Tasks;

namespace LendHand.Resources.Adapter
{
    public interface ILanguageService
    {
        Task<int> Add(string name, string code);
        Task<int> Update(int id, string name, string code);
        Task<int> Update(string code, string name);

        Task<Language> Get(int id);
        Task<Language> Get(string code);
        Task<Language> Delete(int id);
    }
}
