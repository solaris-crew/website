﻿using AutoMapper;
using LendHand.Contracts.Dto.Resource;
using LendHand.Resources.Data.Entities;

namespace LendHand.Resources.Adapter
{
    public class ResourceMappingProfile : Profile
    {
        public ResourceMappingProfile()
        {
            CreateMap<ResourceDto, Resource>()
                .ReverseMap();
        }
    }
}
