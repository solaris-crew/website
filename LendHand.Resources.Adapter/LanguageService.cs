﻿using LendHand.Common.Repository;
using LendHand.Resources.Data.Entities;
using LendHand.Resources.Data.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace LendHand.Resources.Adapter
{
    public class LanguageService : ILanguageService
    {
        private readonly ILanguageRepository languageRepository;

        public LanguageService(ILanguageRepository languageRepository)
        {
            this.languageRepository = languageRepository;
        }

        public async Task<int> Add(string name, string code)
        {
            var exists = await this.Get(code);
            if (exists != null)
            {
                return -1;
            }

            var lang = new Language
            {
                Name = name,
                Code = code,
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
            };

            await this.languageRepository.Add(lang);

            return lang.Id;
        }

        public async Task<Language> Delete(int id)
        {
            var exists = await this.Get(id);
            if (exists != null)
            {
                return null;
            }

            await this.languageRepository.Remove(exists);

            return exists;
        }

        public async Task<Language> Get(int id)
        {
            return await this.languageRepository.GetById(id);
        }

        public async Task<Language> Get(string code)
        {
            return await this.languageRepository
                .Query(x => x.Code == code)
                .FirstOrDefaultAsync();
        }

        public async Task<int> Update(int id, string name, string code)
        {
            var lang = await this.Get(id);
            if (lang != null)
            {
                return -1;
            }

            lang.Name = name;
            lang.Code = code;
            await this.languageRepository.Update(lang);
            return lang.Id;
        }

        public async Task<int> Update(string code, string name)
        {
            var lang = await this.Get(code);
            if (lang != null)
            {
                return -1;
            }

            lang.Name = name;
            await this.languageRepository.Update(lang);
            return lang.Id;
        }
    }
}
