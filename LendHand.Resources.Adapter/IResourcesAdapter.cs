﻿using LendHand.Contracts.Dto.Resource;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendHand.Resources.Adapter
{
    public interface IResourcesAdapter
    {
        Task<ResourceDto> GetResource(string code, int languageId);
        Task<ResourceDto> GetResource(string code, string language);
        Task<ResourceDto> GetResource(int id);
        Task<List<ResourceDto>> GetResource(string code);
        Task<List<ResourceDto>> GetAll(int target = 1);
        Task<List<ResourceDto>> GetAll(int languageId, int target = 1);

        Task<int> Add(ResourceDto resource);
        Task<int> Update(int id, string value);
        Task<int> Update(string code, int languageId, string value);
        Task<int> UpdateBinding(int id, int targetId);
    }
}
