﻿using AutoMapper;
using LendHand.Common.Repository;
using LendHand.Contracts.Dto.Resource;
using LendHand.Resources.Data.Entities;
using LendHand.Resources.Data.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendHand.Resources.Adapter
{
    public class ResourcesAdapter : IResourcesAdapter
    {
        private static readonly int DEFAULT_CULTURE = 1;

        private readonly IRepository<Resource> resourcesRepository;
        private readonly ILanguageRepository languagesRepository;
        private readonly IMapper mapper;

        public ResourcesAdapter(
            IRepository<Resource> resourcesRepository,
            ILanguageRepository languagesRepository,
            IMapper mapper)
        {
            this.resourcesRepository = resourcesRepository;
            this.languagesRepository = languagesRepository;
            this.mapper = mapper;
        }

        public async Task<int> Add(ResourceDto resource)
        {
            var entity = this.mapper.Map<Resource>(resource);
            await this.resourcesRepository.Add(entity);
            return entity.Id;
        }

        public async Task<List<ResourceDto>> GetAll(int target = 1)
        {
            var list = await this.resourcesRepository.Get(x => x.TargetId == target);
            return this.mapper.Map<List<ResourceDto>> (list);
        }

        public async Task<List<ResourceDto>> GetAll(int languageId, int target)
        {
            var list = await this.resourcesRepository
                .Get(x => x.LanguageId == languageId && x.TargetId == target);

            return this.mapper.Map<List<ResourceDto>>(list);
        }

        public async Task<ResourceDto> GetResource(string code, int languageId)
        {
            var resource = await this.GetResourceIternal(code, languageId);

            var result =  resource ?? await this.resourcesRepository
                .Query(x => x.LanguageId == DEFAULT_CULTURE && x.Code == code)
                .FirstOrDefaultAsync();

            return this.mapper.Map<ResourceDto>(result);
        }

        public async Task<ResourceDto> GetResource(string code, string language)
        {
            var lang = await languagesRepository.GetByCode(language);
            var langId = lang?.Id ?? DEFAULT_CULTURE;

            var res = await this.GetResourceIternal(code, langId);

            if (res == null)
            {
                res = await this.GetResourceIternal(code, DEFAULT_CULTURE);
            }

            return this.mapper.Map<ResourceDto>(res);
        }

        public async Task<ResourceDto> GetResource(int id)
        {
            var entity = await this.resourcesRepository.GetById(id);
            return this.mapper.Map<ResourceDto>(entity);
        }

        public async Task<List<ResourceDto>> GetResource(string code)
        {
            var entity = await this.resourcesRepository
                .Get(x => x.Code == code);

            return this.mapper.Map<List<ResourceDto>>(entity);
        }

        public async Task<int> Update(int id, string value)
        {
            var resource = await this.resourcesRepository.GetById(id);
            if (resource == null)
            {
                return -1;
            }

            resource.Value = value;
            await this.resourcesRepository.Update(resource);
            return resource.Id;
        }

        public async Task<int> Update(string code, int languageId, string value)
        {
            var resource = await this.GetResourceIternal(code, languageId);
            if (resource == null)
            {
                return -1;
                //resource = new Resource
                //{
                //    Code = code,
                //    Value = value,
                //    LanguageId = languageId,
                //    CreatedOn = DateTime.UtcNow,
                //    UpdatedOn = DateTime.UtcNow,
                //};

                //return await this.Add(resource);
            }

            resource.Value = value;
            await this.resourcesRepository.Update(resource);
            return resource.Id;
        }

        public async Task<int> UpdateBinding(int id, int targetId)
        {
            var resource = await this.resourcesRepository.GetById(id);
            if (resource == null)
            {
                return -1;
            }

            resource.TargetId = targetId;
            await this.resourcesRepository.Update(resource);
            return resource.Id;
        }

        private async Task<Resource> GetResourceIternal(string code, int languageId)
        {
            return await this.resourcesRepository
                .Query(x => x.LanguageId == languageId && x.Code == code)
                .FirstOrDefaultAsync();
        }
    }
}
