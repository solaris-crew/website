﻿using LendHand.Contracts.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace LendHand.AbstractWeb.StartupConfiguration
{
    public class LendHandStartupBase
    {
        public IConfiguration Configuration { get; }

        public LendHandStartupBase(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            AddSettings(services);
            AddDatabase(services);
            AddServices(services);
            AddMapping(services);
            AddLogging(services);
        }

        protected virtual void AddSettings(IServiceCollection services)
        {
            services.Configure<ConnectionStrings>(
                this.Configuration.GetSection("ConnectionStrings"));
        }

        protected virtual void AddServices(IServiceCollection services)
        {
        }

        protected virtual void AddMapping(IServiceCollection services)
        {
        }

        protected virtual void AddDatabase(IServiceCollection services)
        {

        }

        protected virtual void AddLogging(IServiceCollection services)
        {
            // todo add loggers (with serilog)
        }

        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            // todo exception handler page
            // else
            // {
            //     app.UseExceptionHandler("/Home/Error");
            // }

            app.UseStaticFiles();

            app.UseSerilogRequestLogging();

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(option =>
            {
                option.SwaggerEndpoint("/swagger/v1/swagger.json", "LendHand API v.1.0.0");
                option.RoutePrefix = string.Empty;
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
