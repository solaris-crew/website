﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;

namespace LendHand.AbstractWeb.Extensions
{
    public static class ControllerBaseExtensions
    {
        public static async Task<T> Parse<T>(this ControllerBase cb)
        {
            using var reader = new StreamReader(cb.Request.Body);
            var str = await reader.ReadToEndAsync();
            return JsonConvert.DeserializeObject<T>(str);
        }
    }
}
