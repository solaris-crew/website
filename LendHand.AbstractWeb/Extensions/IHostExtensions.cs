﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;

namespace LendHand.AbstractWeb.Extensions
{
    public static class IHostExtensions
    {
        public static void MigrateDatabase<TContext>(this IHost host)
            where TContext : DbContext
        {
            using var scope = host.Services.CreateScope();
            var context = scope.ServiceProvider.GetRequiredService<TContext>();
            try
            {
                context.Database.Migrate();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Database migration error.");
                Environment.Exit(-1);
            }
        }
    }
}
