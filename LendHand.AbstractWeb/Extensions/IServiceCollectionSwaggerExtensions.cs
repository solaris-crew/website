﻿using LendHand.AbstractWeb.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System.IO;

namespace LendHand.AbstractWeb.Extensions
{
    public static class IServiceCollectionSwaggerExtensions
    {
        public static void ConfigureSwagger(this IServiceCollection services, SwaggerSettingsDto settings)
        {
            var p = services.AddSwaggerGen(option =>
            {
                option.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = settings.Title,
                    Version = "1.0.0"
                });

                // todo versions configuration

                // todo auth configuration

                var xmlFile = $"{settings.XmlAssemblyName}.xml";
                var xmlPath = Path.Combine(settings.XmlBasePath, xmlFile);
                option.IncludeXmlComments(xmlPath);
            });
        }
    }
}
