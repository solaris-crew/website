﻿namespace LendHand.AbstractWeb.Models
{
    /// <summary>
    /// Data model which describes swagger configuration settings for current API project.
    /// </summary>
    public class SwaggerSettingsDto
    {
        /// <summary>
        /// Executing assembly name.
        /// </summary>
        /// <example> Assembly.GetExecutingAssembly().GetName().Name </example>
        public string XmlAssemblyName { get; set; }
        /// <summary>
        /// Executing assembly base path.
        /// </summary>
        /// <example> AppContext.BaseDirectory </example>
        public string XmlBasePath { get; set; }

        /// <summary>
        /// Api description document base title.
        /// </summary>
        public string Title { get; set; }

    }
}
