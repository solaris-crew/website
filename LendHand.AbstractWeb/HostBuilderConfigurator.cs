﻿using LendHand.AbstractWeb.StartupConfiguration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace LendHand.AbstractWeb
{
    public class HostBuilderConfigurator
    {
        public IHostBuilder Configure<TStartup>(string[] args)
            where TStartup : LendHandStartupBase
        {
            var builder = Host
                .CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<TStartup>())
                .UseSerilog();
            return builder;
        }
    }
}
