﻿using LendHand.Common.Repository;
using LendHand.Resources.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LendHand.Resources.Data.Repository.Interfaces
{
    public interface ILanguageRepository : IRepository<Language>
    {
        Task<Language> GetByCode(string code);
    }
}
