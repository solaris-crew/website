﻿using LendHand.Common.Repository;
using LendHand.Resources.Data.Entities;

namespace LendHand.Resources.Data.Repository
{
    public class ResouresRepository : RepositoryBase<Resource>
    {
        public ResouresRepository(ResourcesDbContext context)
            : base(context) { }
    }
}
