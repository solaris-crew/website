﻿using LendHand.Common.Repository;
using LendHand.Resources.Data.Entities;
using LendHand.Resources.Data.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace LendHand.Resources.Data.Repository
{
    public class LanguageRepository : RepositoryBase<Language>, ILanguageRepository
    {
        public LanguageRepository(ResourcesDbContext context)
            : base(context) { }

        public async Task<Language> GetByCode(string code)
        {
            var lang = await this.Set.Where(x => x.Code == code).FirstOrDefaultAsync();
            return lang;
        }
    }
}
