﻿using LendHand.Common.Data;

namespace LendHand.Resources.Data.Entities
{
    public class Resource : Entity
    {
        public string Code { get; set; }
        public string Value { get; set; }
        public int LanguageId { get; set; } = 1;
        public int TargetId { get; set; } = 1;

        public Language Language { get; set; }
        public Target Target { get; set; }
    }
}
