﻿using LendHand.Common.Data;

namespace LendHand.Resources.Data.Entities
{
    public class Target : Entity
    {
        public string Name { get; set; }
    }
}