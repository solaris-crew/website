﻿using LendHand.Common.Data;

namespace LendHand.Resources.Data.Entities
{
    public class Language : Entity
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}