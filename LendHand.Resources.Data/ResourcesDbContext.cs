﻿using LendHand.Resources.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace LendHand.Resources.Data
{
    public class ResourcesDbContext : DbContext
    {
        public ResourcesDbContext(
            [NotNull]DbContextOptions<ResourcesDbContext> options)
            : base(options) { }

        public virtual DbSet<Language> Language { get; set; }
        public virtual DbSet<Resource> Resource { get; set; }
        public virtual DbSet<Target> Target { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Resource>()
                .HasIndex("Code", "LanguageId")
                .IsUnique();

            modelBuilder.Entity<Target>()
                .HasData(new Target { Id = Constants.ServiceType.WEBSITE_TARGET_ID, Name = "website" });
            modelBuilder.Entity<Target>()
                .HasData(new Target { Id = Constants.ServiceType.TELEGRAM_TARGET_ID, Name = "telegram" });
                        
            modelBuilder.Entity<Language>()
                .HasData(
                new Language { Id = 1, Name = "English", Code = "en" },
                new Language { Id = 2, Name = "Русский", Code = "ru" },
                new Language { Id = 3, Name = "Українська", Code = "uk" },
                new Language { Id = 4, Name = "Deutsche", Code = "de" },
                new Language { Id = 5, Name = "Italiano", Code = "it" });


            modelBuilder.Entity<Resource>()
                .HasData(
                // telegram messages end
                new Resource
                {
                    Id = 1000,
                    Code = "message_hello",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"Hello. Welcome to Lend-a-Hand bot!"
                },
                new Resource
                {
                    Id = 1001,
                    Code = "button_sign_in_volunteer",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = "Volunteer"
                },
                new Resource
                {
                    Id = 1002,
                    Code = "button_sign_in_needy",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = "Request help"
                },
                new Resource
                {
                    Id = 1003,
                    Code = "button_sign_in_org",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = "Organization"
                },
                new Resource
                {
                    Id = 1004,
                    Code = "button_policy",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = "Read policy"
                },
                new Resource
                {
                    Id = 1005,
                    Code = "message_hello_1",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"Please, read policy below.
By continuing using this bot, you agree to our terms and condition listed in our policy."
                },
                new Resource
                {
                    Id = 1006,
                    Code = "message_hello_2",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"If you want to lend a hand, you can sign in as a volunteer or as an organization:"
                },
                new Resource
                {
                    Id = 1007,
                    Code = "message_hello_3",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"If you need help, please submit a request:"
                },
                new Resource
                {
                    Id = 1008,
                    Code = "message_hello_org",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"Enter country where your organization is registred."
                },
                new Resource
                {
                    Id = 1009,
                    Code = "button_sign_in_org_send_country",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"Submit country"
                },
                new Resource
                {
                    Id = 1010,
                    Code = "cancel",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"Cancel"
                },

                // telegram messages ua
                new Resource
                {
                    Id = 1011,
                    Code = "message_hello",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"Привіт! Ласкаво просимо до боту Lend-a-Hand!"
                },
                new Resource
                {
                    Id = 1012,
                    Code = "button_sign_in_volunteer",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = "Волонтер"
                },
                new Resource
                {
                    Id = 1013,
                    Code = "button_sign_in_needy",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = "Звернутися за допомогою"
                },
                new Resource
                {
                    Id = 1014,
                    Code = "button_sign_in_org",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = "Організація"
                },
                new Resource
                {
                    Id = 1015,
                    Code = "button_policy",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = "Наша політика"
                },
                new Resource
                {
                    Id = 1016,
                    Code = "message_hello_1",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"Будь ласка, прочитайте наші правила та політику за посиланням нижче.
Продовживши використовувати бота, Ви автоматично погоджуєтись з правилами та умовами використання, що описані в нашій політиці."
                },
                new Resource
                {
                    Id = 1017,
                    Code = "message_hello_2",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"Якщо Ви хочете протягнути руку допомоги, Ви можете зареєструватися як волонтер або як організація:"
                },
                new Resource
                {
                    Id = 1018,
                    Code = "message_hello_3",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"Якщо вам потрібна допомога, підтвердіть запит:"
                },
                new Resource
                {
                    Id = 1019,
                    Code = "message_hello_org",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"Введіть назву країни, в якій зареєстрована Ваша організація:"
                },
                new Resource
                {
                    Id = 1020,
                    Code = "button_sign_in_org_send_country",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"Підтвердити країну"
                },
                new Resource
                {
                    Id = 1021,
                    Code = "cancel",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"Відмінити"
                },

                // telegram messages de
                new Resource
                {
                    Id = 1022,
                    Code = "message_hello",
                    LanguageId = 4,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"Hallo! Willkommen in Lend-a-Hand-Bot!"
                },

                // telegram messages it
                new Resource
                {
                    Id = 1023,
                    Code = "message_hello",
                    LanguageId = 5,
                    TargetId = Constants.ServiceType.TELEGRAM_TARGET_ID,
                    Value = @"Ciao! Benvenuto nel bot Lend-a-Hand!"
                },



                // web site eng
                new Resource
                {
                    Id = 16,
                    Code = "banner_section_text_1",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "Lend your hand and start making difference using messenger of your choice now!"
                },
                new Resource
                {
                    Id = 17,
                    Code = "banner_section_text_2",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "Need assistance in delivering food and medicine? Ask volunteers to help just with a couple of clicks!"
                },
                new Resource
                {
                    Id = 18,
                    Code = "one_section_header_1",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "ABOUT OUR PROJECT"
                },
                new Resource
                {
                    Id = 19,
                    Code = "one_section_text_1",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = @"Lend-a-Hand is a cloud-based direct volunteering system that manages volunteering assistance in food and medicine delivery for isolated people requiring help.
It allows them to submit a request for volunteers to pick up, assigns a volunteer, and establishes a connection between parties to solve the details of the request.
It is fast, reliable and convenient.
We co - operate with various volunteering organizations and initiatives to direct their — and anyone’s — help where it is needed the most.
Everybody — meaning you too — can lend their hand and put their effort into fighting COVID - 19 and helping people under its impact."
                },
                new Resource
                {
                    Id = 20,
                    Code = "two_section_header_1",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "WHAT'S NEXT?"
                },
                new Resource
                {
                    Id = 21,
                    Code = "two_section_text_1",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = @"While we are still looking forward to our service to start operating, we are constantly developing it and trying to implement more features.
First step we expect to make is to create a secure way to verify and engage new non-aligned volunteers, making registration and using the bot safer and more convenient.
We are also expanding, assisting people in new cities and regions. We put efforts into translating our system as well as adjusting it to new needs and challenges.
Also, as we are non-profit project, we look for angels and funders that will help us fighting COVID-19 and helping people around the globe!"
                },
                new Resource
                {
                    Id = 22,
                    Code = "layout_header_1_nav_1",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "HOME"
                },
                new Resource
                {
                    Id = 23,
                    Code = "layout_header_1_nav_2",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "ABOUT OUR PROJECT"
                },
                new Resource
                {
                    Id = 24,
                    Code = "layout_header_1_nav_3",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "WHAT'S NEXT"
                },
                new Resource
                {
                    Id = 25,
                    Code = "layout_header_1_nav_4",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "CONTACT US"
                },
                new Resource
                {
                    Id = 26,
                    Code = "footer_section_header_1",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "CONTACT US"
                },
                new Resource
                {
                    Id = 27,
                    Code = "footer_section_text_1",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = @"One of the ways you can help us the most, save for using our project directly, is to give us feedback. Comments, experience, advices and worries — all these are very much welcome, for it is the only way for us to be build a truly convenient and viable solution.
If you have skills and experience in similar projects and you want to assist our small team — write to us and we will be glad for you to join.We are going to start putting exact requirements and terms soon.
If you intersted in supporting our project with funding, please contact us.Soon we also expect to launch a crowdfunding campaign, so stay in touch!"
                },
                new Resource
                {
                    Id = 28,
                    Code = "footer_section_address_header_1",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = @"Address"
                },
                new Resource
                {
                    Id = 29,
                    Code = "footer_section_mail_header_1",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = @"Email"
                },
                new Resource
                {
                    Id = 30,
                    Code = "footer_section_phone_header_1",
                    LanguageId = Constants.Language.ENGLISH_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = @"Phone"
                },

                // web site ua
                new Resource
                {
                    Id = 31,
                    Code = "banner_section_text_1",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "Протягніть руку і почніть вносити зміни, використовуючи месенджер на ваш вибір прямо зараз!"
                },
                new Resource
                {
                    Id = 32,
                    Code = "banner_section_text_2",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "Вам потрібна допомога в доставці продуктів харчування або ліків? Зверніться до волонтерів за допомогою лише кількома кліками!"
                },
                new Resource
                {
                    Id = 33,
                    Code = "one_section_header_1",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "ПРО НАШ ПРОЕКТ"
                },
                new Resource
                {
                    Id = 34,
                    Code = "one_section_text_1",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = @"Lend-a-Hand — це хмарний сервіс для адресної волонтерської допомоги, що забезпечує волонтерську доставку їжі та медикаментів людям, що потребують допомоги.
Він дозволяє їм надіслати запит про допомогу до волонтерів, призначає виконавця та налагоджує зв’язок між сторонами для узгодження деталей запиту.
Він швидкий, надійний та зручний.
Ми співпрацюємо з численними волонтерськими організаціями та ініціятивами щоб спрямовувати їхню — й будь-чию — допомогу туди, де її потребують найбільше.
Кожен — тобто й ви самі — може простягнути руку допомоги та докласти своїх зусиль до боротьби з COVID-19 та допомоги постраждалим від нього."
                },
                new Resource
                {
                    Id = 35,
                    Code = "two_section_header_1",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "ЩО ДАЛІ?"
                },
                new Resource
                {
                    Id = 36,
                    Code = "two_section_text_1",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = @"Попри те, що ми ще чекаємо на запуск свого сервісу, ми постійно вдосконалюємо його, додаючи нові засоби та можливості.
В першу чергу ми плануємо впровадити безпечний спосіб зробити наш сервіс доступним індивідуальним волонтерам та розробити Положення безпеки, зробивши нашу системою безпечнішою та зручнішою.
Ми також розширюємо покриття нашого сервісу, охоплюючи нові міста та місцевості. Ми докладаємо зусиль, щоб зробити нашу систему доступною іншими мовами та пристосованою до нових викликів.
Будучи неприбутковим проектом, ми шукаємо янголів та меценатів, готових підтримати нас у боротьбі з COVID-19!"
                },
                new Resource
                {
                    Id = 37,
                    Code = "layout_header_1_nav_1",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "ДОМАШНЯ СТОРІНКА"
                },
                new Resource
                {
                    Id = 38,
                    Code = "layout_header_1_nav_2",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "ПРО НАШ ПРОЕКТ"
                },
                new Resource
                {
                    Id = 39,
                    Code = "layout_header_1_nav_3",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "ЩО ДАЛІ?"
                },
                new Resource
                {
                    Id = 40,
                    Code = "layout_header_1_nav_4",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "КОНТАКТИ"
                },
                new Resource
                {
                    Id = 41,
                    Code = "footer_section_header_1",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "КОНТАКТИ"
                },
                new Resource
                {
                    Id = 42,
                    Code = "footer_section_text_1",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = @"Один із способів, яким ви можете нам найбільше допомогти, окрім використання безпосередньо нашого проекту, - це надати нам відгуки. Коментарі, досвід, поради та турбота - все це дуже вітається, адже це єдиний спосіб для нас побудувати справді зручне та життєздатне рішення.
Якщо у вас є навички та досвід у подібних проектах та ви хочете допомогти нашій невеликій команді - пишіть нам, і ми будемо раді, якщо ви приєднаєтесь. Ми скоро розпочнемо викладати точні правила та умови.
Якщо ви зацікавлені в підтримці нашого проекту фінансуванням, будь ласка, зв’яжіться з нами. Ми також розраховуємо на те, що скоро ми розпочнемо краудфандингову кампанію, тому будьте в курсі!"
                },
                new Resource
                {
                    Id = 43,
                    Code = "footer_section_address_header_1",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "Адреса"
                },
                new Resource
                {
                    Id = 44,
                    Code = "footer_section_mail_header_1",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "Електронна пошта"
                },
                new Resource
                {
                    Id = 45,
                    Code = "footer_section_phone_header_1",
                    LanguageId = Constants.Language.UKRAINE_LANGUAGE_ID,
                    TargetId = Constants.ServiceType.WEBSITE_TARGET_ID,
                    Value = "Телефон"
                }
            );
        }
    }
}
