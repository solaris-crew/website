﻿namespace LendHand.Resources.Data
{
    public static class Constants
    {
        public static class ServiceType
        {
            public static readonly int WEBSITE_TARGET_ID = 1;
            public static readonly int TELEGRAM_TARGET_ID = 2;
        }
        public static class Language
        {
            public static readonly int ENGLISH_LANGUAGE_ID = 1;
            public static readonly int RUSSIAN_LANGUAGE_ID = 2;
            public static readonly int UKRAINE_LANGUAGE_ID = 3;
        }
    }
}
