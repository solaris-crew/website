﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LendHand.Resources.Data.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Language",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Language", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Target",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Target", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Resource",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true),
                    LanguageId = table.Column<int>(nullable: false),
                    TargetId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resource", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Resource_Language_LanguageId",
                        column: x => x.LanguageId,
                        principalTable: "Language",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Resource_Target_TargetId",
                        column: x => x.TargetId,
                        principalTable: "Target",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Language",
                columns: new[] { "Id", "Code", "CreatedOn", "Name", "UpdatedOn" },
                values: new object[] { 1, "en", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "English", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Language",
                columns: new[] { "Id", "Code", "CreatedOn", "Name", "UpdatedOn" },
                values: new object[] { 2, "ru", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Русский", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Language",
                columns: new[] { "Id", "Code", "CreatedOn", "Name", "UpdatedOn" },
                values: new object[] { 3, "uk", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Українська", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Language",
                columns: new[] { "Id", "Code", "CreatedOn", "Name", "UpdatedOn" },
                values: new object[] { 4, "de", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Deutsche", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Language",
                columns: new[] { "Id", "Code", "CreatedOn", "Name", "UpdatedOn" },
                values: new object[] { 5, "it", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Italiano", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Target",
                columns: new[] { "Id", "CreatedOn", "Name", "UpdatedOn" },
                values: new object[] { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "website", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Target",
                columns: new[] { "Id", "CreatedOn", "Name", "UpdatedOn" },
                values: new object[] { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "telegram", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 16, "banner_section_text_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Lend your hand and start making difference using messenger of your choice now!" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 45, "footer_section_phone_header_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Телефон" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1000, "message_hello", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Hello. Welcome to Lend-a-Hand bot!" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1001, "button_sign_in_volunteer", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Volunteer" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1002, "button_sign_in_needy", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Request help" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1003, "button_sign_in_org", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Organization" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1004, "button_policy", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Read policy" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1005, "message_hello_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), @"Please, read policy below.
By continuing using this bot, you agree to our terms and condition listed in our policy." });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1006, "message_hello_2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "If you want to lend a hand, you can sign in as a volunteer or as an organization:" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1007, "message_hello_3", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "If you need help, please submit a request:" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1008, "message_hello_org", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Enter country where your organization is registred." });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1009, "button_sign_in_org_send_country", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Submit country" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1010, "cancel", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Cancel" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1011, "message_hello", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Привіт! Ласкаво просимо до боту Lend-a-Hand!" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1012, "button_sign_in_volunteer", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Волонтер" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1013, "button_sign_in_needy", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Звернутися за допомогою" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1014, "button_sign_in_org", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Організація" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1015, "button_policy", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Наша політика" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1016, "message_hello_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), @"Будь ласка, прочитайте наші правила та політику за посиланням нижче.
Продовживши використовувати бота, Ви автоматично погоджуєтись з правилами та умовами використання, що описані в нашій політиці." });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1017, "message_hello_2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Якщо Ви хочете протягнути руку допомоги, Ви можете зареєструватися як волонтер або як організація:" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1018, "message_hello_3", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Якщо вам потрібна допомога, підтвердіть запит:" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1019, "message_hello_org", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Введіть назву країни, в якій зареєстрована Ваша організація:" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1020, "button_sign_in_org_send_country", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Підтвердити країну" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1021, "cancel", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Відмінити" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 44, "footer_section_mail_header_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Електронна пошта" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 43, "footer_section_address_header_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Адреса" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 42, "footer_section_text_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), @"Один із способів, яким ви можете нам найбільше допомогти, окрім використання безпосередньо нашого проекту, - це надати нам відгуки. Коментарі, досвід, поради та турбота - все це дуже вітається, адже це єдиний спосіб для нас побудувати справді зручне та життєздатне рішення.
Якщо у вас є навички та досвід у подібних проектах та ви хочете допомогти нашій невеликій команді - пишіть нам, і ми будемо раді, якщо ви приєднаєтесь. Ми скоро розпочнемо викладати точні правила та умови.
Якщо ви зацікавлені в підтримці нашого проекту фінансуванням, будь ласка, зв’яжіться з нами. Ми також розраховуємо на те, що скоро ми розпочнемо краудфандингову кампанію, тому будьте в курсі!" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 41, "footer_section_header_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "КОНТАКТИ" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 17, "banner_section_text_2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Need assistance in delivering food and medicine? Ask volunteers to help just with a couple of clicks!" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 18, "one_section_header_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ABOUT OUR PROJECT" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 19, "one_section_text_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), @"Lend-a-Hand is a cloud-based direct volunteering system that manages volunteering assistance in food and medicine delivery for isolated people requiring help.
It allows them to submit a request for volunteers to pick up, assigns a volunteer, and establishes a connection between parties to solve the details of the request.
It is fast, reliable and convenient.
We co - operate with various volunteering organizations and initiatives to direct their — and anyone’s — help where it is needed the most.
Everybody — meaning you too — can lend their hand and put their effort into fighting COVID - 19 and helping people under its impact." });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 20, "two_section_header_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "WHAT'S NEXT?" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 21, "two_section_text_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), @"While we are still looking forward to our service to start operating, we are constantly developing it and trying to implement more features.
First step we expect to make is to create a secure way to verify and engage new non-aligned volunteers, making registration and using the bot safer and more convenient.
We are also expanding, assisting people in new cities and regions. We put efforts into translating our system as well as adjusting it to new needs and challenges.
Also, as we are non-profit project, we look for angels and funders that will help us fighting COVID-19 and helping people around the globe!" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 22, "layout_header_1_nav_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "HOME" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 23, "layout_header_1_nav_2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ABOUT OUR PROJECT" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 24, "layout_header_1_nav_3", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "WHAT'S NEXT" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 25, "layout_header_1_nav_4", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "CONTACT US" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 26, "footer_section_header_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "CONTACT US" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 27, "footer_section_text_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), @"One of the ways you can help us the most, save for using our project directly, is to give us feedback. Comments, experience, advices and worries — all these are very much welcome, for it is the only way for us to be build a truly convenient and viable solution.
If you have skills and experience in similar projects and you want to assist our small team — write to us and we will be glad for you to join.We are going to start putting exact requirements and terms soon.
If you intersted in supporting our project with funding, please contact us.Soon we also expect to launch a crowdfunding campaign, so stay in touch!" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1022, "message_hello", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 4, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Hallo! Willkommen beim Lend-a-Hand-Bot!" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 28, "footer_section_address_header_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Address" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 30, "footer_section_phone_header_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Phone" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 31, "banner_section_text_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Протягніть руку і почніть вносити зміни, використовуючи месенджер на ваш вибір прямо зараз!" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 32, "banner_section_text_2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Вам потрібна допомога в доставці продуктів харчування або ліків? Зверніться до волонтерів за допомогою лише кількома кліками!" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 33, "one_section_header_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ПРО НАШ ПРОЕКТ" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 34, "one_section_text_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), @"Lend-a-Hand — це хмарний сервіс для адресної волонтерської допомоги, що забезпечує волонтерську доставку їжі та медикаментів людям, що потребують допомоги.
Він дозволяє їм надіслати запит про допомогу до волонтерів, призначає виконавця та налагоджує зв’язок між сторонами для узгодження деталей запиту.
Він швидкий, надійний та зручний.
Ми співпрацюємо з численними волонтерськими організаціями та ініціятивами щоб спрямовувати їхню — й будь-чию — допомогу туди, де її потребують найбільше.
Кожен — тобто й ви самі — може простягнути руку допомоги та докласти своїх зусиль до боротьби з COVID-19 та допомоги постраждалим від нього." });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 35, "two_section_header_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ЩО ДАЛІ?" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 36, "two_section_text_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), @"Попри те, що ми ще чекаємо на запуск свого сервісу, ми постійно вдосконалюємо його, додаючи нові засоби та можливості.
В першу чергу ми плануємо впровадити безпечний спосіб зробити наш сервіс доступним індивідуальним волонтерам та розробити Положення безпеки, зробивши нашу системою безпечнішою та зручнішою.
Ми також розширюємо покриття нашого сервісу, охоплюючи нові міста та місцевості. Ми докладаємо зусиль, щоб зробити нашу систему доступною іншими мовами та пристосованою до нових викликів.
Будучи неприбутковим проектом, ми шукаємо янголів та меценатів, готових підтримати нас у боротьбі з COVID-19!" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 37, "layout_header_1_nav_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ДОМАШНЯ СТОРІНКА" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 38, "layout_header_1_nav_2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ПРО НАШ ПРОЕКТ" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 39, "layout_header_1_nav_3", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "ЩО ДАЛІ?" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 40, "layout_header_1_nav_4", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "КОНТАКТИ" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 29, "footer_section_mail_header_1", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Email" });

            migrationBuilder.InsertData(
                table: "Resource",
                columns: new[] { "Id", "Code", "CreatedOn", "LanguageId", "TargetId", "UpdatedOn", "Value" },
                values: new object[] { 1023, "message_hello", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 5, 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ciao! Benvenuto nel bot Lend-a-Hand!" });

            migrationBuilder.CreateIndex(
                name: "IX_Resource_LanguageId",
                table: "Resource",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_Resource_TargetId",
                table: "Resource",
                column: "TargetId");

            migrationBuilder.CreateIndex(
                name: "IX_Resource_Code_LanguageId",
                table: "Resource",
                columns: new[] { "Code", "LanguageId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Resource");

            migrationBuilder.DropTable(
                name: "Language");

            migrationBuilder.DropTable(
                name: "Target");
        }
    }
}
