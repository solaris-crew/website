﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LendHand.Resources.Data.Migrations
{
    public partial class de_hollo_fix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 1022,
                column: "Value",
                value: "Hallo! Willkommen in Lend-a-Hand-Bot!");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Resource",
                keyColumn: "Id",
                keyValue: 1022,
                column: "Value",
                value: "Hallo! Willkommen beim Lend-a-Hand-Bot!");
        }
    }
}
