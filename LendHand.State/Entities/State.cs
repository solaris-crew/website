﻿using LendHand.Common.Data;

namespace LendHand.State.Entities
{
    public class State : Entity
    {
        public long TelegramId { get; set; }
        public string StateCode { get; set; }
    }
}
