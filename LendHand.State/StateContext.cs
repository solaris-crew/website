﻿using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace LendHand.State
{
    public class StateContext : DbContext
    {
        public virtual DbSet<Entities.State> State { get; set; }

        public StateContext(
            [NotNull]DbContextOptions<StateContext> options)
            : base(options) { }
    }
}
