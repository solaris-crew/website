﻿using LendHand.Common.Repository;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace LendHand.State.Services
{
    public class StateService : IStateService
    {
        private readonly IRepository<Entities.State> repository;
        
        public StateService(IRepository<Entities.State> repository)
        {
            this.repository = repository;
        }

        public async Task<int> AddState(Entities.State state)
        {
            await this.repository.Add(state);
            return state.Id;
        }

        public async Task<Entities.State> Get(long telegramId)
        {
            return await repository
                .Query(x => x.TelegramId == telegramId)
                .FirstOrDefaultAsync();
        }

        public async Task<Entities.State> GetById(int id)
        {
            return await repository.GetById(id);
        }

        public async Task<Entities.State> Remove(long chatId)
        {
            var state = await this.Get(chatId);
            await this.repository.Remove(state);
            return state;
        }

        public async Task<int> UpdateState(Entities.State state)
        {
            await this.repository.Update(state);
            return state.Id;
        }
    }
}
