﻿using LendHand.Common.Repository;

namespace LendHand.State.Services
{
    public class StateRepository : RepositoryBase<Entities.State>
    {
        public StateRepository(StateContext context)
            : base(context) { }
    }
}
