﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LendHand.State.Services
{
    public interface IStateService
    {
        Task<int> AddState(Entities.State state);
        Task<int> UpdateState(Entities.State state);
        Task<Entities.State> Get(long telegramId);
        Task<Entities.State> GetById(int id);
        Task<Entities.State> Remove(long chatId);
    }
}
