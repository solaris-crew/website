﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LendHand.Telegram.Business.BotClient
{
    public class TelegramBotSettings
    {
        public string WebHookUrl { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }
    }
}
