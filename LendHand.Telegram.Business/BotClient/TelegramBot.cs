﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;

namespace LendHand.Telegram.Business.BotClient
{
    public class TelegramBot
    {
        public static TelegramBotClient BotClient { get; private set; }

        /// <summary>
        /// Create and return telegram client
        /// </summary>
        /// <returns>Static telegram client</returns>
        public static async Task<TelegramBotClient> GetBotClientAsync(TelegramBotSettings options)
        {
            if (BotClient != null)
            {
                return BotClient;
            }

            BotClient = new TelegramBotClient(options.Token);
            string hook = string.Format(options.WebHookUrl, "api/message/update");
            await BotClient.SetWebhookAsync(hook);
            return BotClient;
        }
    }
}
