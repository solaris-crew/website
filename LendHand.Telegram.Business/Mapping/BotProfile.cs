﻿using AutoMapper;
using LendHand.Contracts.Dto.Bot;
using LendHand.Telegram.Data.Entities;

namespace LendHand.Telegram.Business.Mapping
{
    public class BotProfile : Profile
    {
        public BotProfile()
        {
            ShouldMapField = fieldInfo => true;
            ShouldMapProperty = propertyInfo => true;

            CreateMap<Volunteer, VolunteerDto>()
                .ReverseMap();
            CreateMap<Needy, NeedyDto>()
                .ReverseMap();
            CreateMap<Invocation, InvocationDto>()
                .ReverseMap();
            CreateMap<Invocation, AddInvocationDto>()
                .ReverseMap();
        }
    }
}
