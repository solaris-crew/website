﻿using LendHand.Resources.Adapter;
using LendHand.Telegram.Business.Commands.Extensions;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using static LendHand.Telegram.Business.Commands.CodeConstants;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;

namespace LendHand.Telegram.Business.Commands
{
    public class StartCommand : CommandBase
    {

        protected IResourcesAdapter resources;

        public StartCommand(IResourcesAdapter adapter) : base(adapter)
        {
            this.resources = adapter;
        }

        public async override Task<bool> ExecuteAsync(Update update)
        {
            return await this.InvokeNextAsync(update);
        }

        protected override async Task<bool> InvokeNextAsync(Update update)
        {
            var chatId = update.CallbackQuery?.From.Id ?? update.Message.From.Id;
            var lang = update.CallbackQuery?.From.LanguageCode ?? update.Message.From.LanguageCode;

            var message_hello = await GetLocalizeTextAsync(LocStringCodes.MESSAGE_HELLO, lang);
            var message_hello_1 = await GetLocalizeTextAsync(LocStringCodes.MESSAGE_HELLO_1, lang);
            var message_hello_2 = await GetLocalizeTextAsync(LocStringCodes.MESSAGE_HELLO_2, lang);
            var message_hello_3 = await GetLocalizeTextAsync(LocStringCodes.MESSAGE_HELLO_3, lang);

            var volunteer_button_caption = await GetLocalizeTextAsync(LocStringCodes.SIGN_VOLUNTEER_BUTTON, lang);
            var needy_button_caption = await GetLocalizeTextAsync(LocStringCodes.SIGN_NEEDY_BUTTON, lang);
            var org_button_caption = await GetLocalizeTextAsync(LocStringCodes.SIGN_ORG_BUTTON, lang);
            var policy_button_caption = await GetLocalizeTextAsync(LocStringCodes.POLICY_BUTTON, lang);

            var volunteer_button = MessageExtensions
                .GetInlineButton(volunteer_button_caption, CommandCodes.SIGN_IN_VOLUNTEER);
            var needy_button = MessageExtensions
                .GetInlineButton(needy_button_caption, CommandCodes.SIGN_IN_NEEDY);
            var org_button = MessageExtensions
                .GetInlineButton(org_button_caption, CommandCodes.SIGN_IN_ORG);
            var policy_button = MessageExtensions
                .GetInlineButtonUrl(policy_button_caption, "https://google.com");
            var markup1 = MessageExtensions.GetInlineMarkup(policy_button);
            var markup2 = MessageExtensions.GetInlineMarkup(org_button, volunteer_button);
            var markup3 = MessageExtensions.GetInlineMarkup(needy_button);

            await this.TelegramClient.SendTextMessageAsync(chatId, message_hello);
            await this.TelegramClient.SendTextMessageAsync(chatId, message_hello_1, replyMarkup: markup1);
            await this.TelegramClient.SendTextMessageAsync(chatId, message_hello_2, replyMarkup: markup2);
            await this.TelegramClient.SendTextMessageAsync(chatId, message_hello_3, replyMarkup: markup3);

            return await Task.FromResult(true);
        }
    }
}
