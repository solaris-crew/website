﻿using LendHand.Resources.Adapter;
using LendHand.Telegram.Business.Commands.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using static LendHand.Telegram.Business.Commands.CodeConstants;

namespace LendHand.Telegram.Business.Commands
{
    public class ErrorCommand : CommandBase
    {

        protected IResourcesAdapter resources;

        public ErrorCommand(IResourcesAdapter adapter) : base(adapter)
        {
            this.resources = adapter;
        }

        public async override Task<bool> ExecuteAsync(Update update)
        {
            return await this.InvokeNextAsync(update);
        }

        protected override async Task<bool> InvokeNextAsync(Update update)
        {
            var chatId = update.CallbackQuery?.From.Id ?? update.Message.From.Id;
            var lang = update.CallbackQuery?.From.LanguageCode ?? update.Message.From.LanguageCode;

            // var message_hello = await GetLocalizeTextAsync(LocStringCodes.MESSAGE_HELLO, lang);

            var button = MessageExtensions
                .GetInlineButton("Restart bot", CommandCodes.RESTART_ALL);

            var markup = MessageExtensions.GetInlineMarkup(button);

            await this.TelegramClient.SendTextMessageAsync(chatId, "Error: wrong action.", replyMarkup: markup);

            return await Task.FromResult(true);
        }
    }
}