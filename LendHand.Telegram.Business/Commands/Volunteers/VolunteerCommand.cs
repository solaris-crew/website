﻿using LendHand.Contracts.Dto.Bot;
using LendHand.Resources.Adapter;
using LendHand.Telegram.Business.Commands.Extensions;
using LendHand.Telegram.Business.Services.Interfaces;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using static LendHand.Telegram.Business.Commands.CodeConstants;

namespace LendHand.Telegram.Business.Commands
{
    public class VolunteerCommand : CommandBase
    {
        protected IResourcesAdapter resources;
        protected IVolunteerService volunteerService;

        public VolunteerCommand(
            IResourcesAdapter adapter,
            IVolunteerService volunteerService) : base(adapter)
        {
            this.resources = adapter;
            this.volunteerService = volunteerService;
        }

        public async override Task<bool> ExecuteAsync(Update update)
        {

            await volunteerService.Add(new VolunteerDto
            {
                TelegramChatId = update.CallbackQuery.Message.From.Id,
                IsPoliciesAccepted = true
                //TODO LangId
            });
            return await this.InvokeNextAsync(update);
        }

        protected override async Task<bool> InvokeNextAsync(Update update)
        {
            var chatId = update.CallbackQuery.Message.Chat.Id;
            var lang = update.CallbackQuery.From.LanguageCode;


            var buttonMessage = await GetLocalizeTextAsync(LocStringCodes.ACTIVE_BUTTON, lang);
            var headerMessage = await GetLocalizeTextAsync(LocStringCodes.ACTIVE_MESSAGE, lang);

            var buttonEntity = MessageExtensions.GetInlineButton(buttonMessage, CommandCodes.ACTIVE);
            var markupEntity = MessageExtensions.GetInlineMarkup(buttonEntity);

            await this.TelegramClient.SendTextMessageAsync(chatId, headerMessage, replyMarkup: markupEntity);

            return await Task.FromResult(true);
        }
    }
}
