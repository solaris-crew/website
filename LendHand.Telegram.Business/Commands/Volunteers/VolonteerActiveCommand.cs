﻿using LendHand.Resources.Adapter;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using static LendHand.Telegram.Business.Commands.CodeConstants;

namespace LendHand.Telegram.Business.Commands
{
    public class VolonteerActiveCommand : CommandBase
    {
        protected IResourcesAdapter resources;

        public VolonteerActiveCommand(
            IResourcesAdapter adapter) : base(adapter)
        {
            this.resources = adapter;
        }

        public async override Task<bool> ExecuteAsync(Update update)
        {
            return await this.InvokeNextAsync(update);
        }

        protected override async Task<bool> InvokeNextAsync(Update update)
        {
            var chatId = update.CallbackQuery.Message.Chat.Id;
            var lang = update.CallbackQuery.From.LanguageCode;

            var message = await GetLocalizeTextAsync(LocStringCodes.GET_LOCATION_MESSAGE, lang);

            await this.TelegramClient.SendTextMessageAsync(chatId, message);

            return await Task.FromResult(true);
        }
    }
}
