﻿using LendHand.Contracts.Dto.Bot;
using LendHand.Resources.Adapter;
using LendHand.Telegram.Business.Commands.Extensions;
using LendHand.Telegram.Business.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using static LendHand.Telegram.Business.Commands.CodeConstants;

namespace LendHand.Telegram.Business.Commands
{
    public class LocationCommand
    {
        IResourcesAdapter resources;
        IInvocationService invocationService;
        IVolunteerService volunteerService;
        INeedyService needyService;

        public LocationCommand(
            IResourcesAdapter adapter,
            IInvocationService invocationService,
            IVolunteerService volunteerService,
            INeedyService needyService
            ) : base()
        {
            this.resources = adapter;
            this.invocationService = invocationService;
            this.volunteerService = volunteerService;
            this.needyService = needyService;
        }

        public async Task<string> ExecuteAsync(Update update)
        {
            var needy = await needyService.Get(update.Message.From.Id);
            if(needy != null)
            {
                return "";
            }
            return await this.GetInvokeNext(update);
        }

        protected async Task<string> GetInvokeNext(Update update)
        {
            //TODO
            return await Task.FromResult("");
        }
    }
}
