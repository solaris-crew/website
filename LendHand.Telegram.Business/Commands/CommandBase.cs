﻿using LendHand.Resources.Adapter;
using LendHand.Telegram.Business.BotClient;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace LendHand.Telegram.Business.Commands
{
    public abstract class CommandBase
    {
        protected ITelegramBotClient TelegramClient => TelegramBot.BotClient;

        protected readonly IResourcesAdapter resourcesAdapter;

        public CommandBase(IResourcesAdapter resourcesAdapter)
        {
            this.resourcesAdapter = resourcesAdapter;
        }

        public abstract Task<bool> ExecuteAsync(Update update);
        protected abstract Task<bool> InvokeNextAsync(Update update);

        protected virtual async Task<string> GetLocalizeTextAsync(string code, string lang)
        {
            var message = await resourcesAdapter.GetResource(code, lang);
            return message.Value;
        }
    }
}
