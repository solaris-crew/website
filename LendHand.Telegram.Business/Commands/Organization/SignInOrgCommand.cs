﻿using LendHand.Resources.Adapter;
using LendHand.Telegram.Business.Commands.Extensions;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using static LendHand.Telegram.Business.Commands.CodeConstants;

namespace LendHand.Telegram.Business.Commands.Organization
{
    public class SignInOrgCommand : CommandBase
    {
        protected IResourcesAdapter resources;

        public SignInOrgCommand(IResourcesAdapter adapter) : base(adapter)
        {
            this.resources = adapter;
        }

        public async override Task<bool> ExecuteAsync(Update update)
        {
            return await this.InvokeNextAsync(update);
        }

        protected override async Task<bool> InvokeNextAsync(Update update)
        {
            var chatId = update.CallbackQuery?.From.Id ?? update.Message.From.Id;
            var lang = update.CallbackQuery?.From.LanguageCode ?? update.Message.From.LanguageCode;

            var message_hello = await GetLocalizeTextAsync(LocStringCodes.HELLO_ORG_MESSAGE, lang);
            // var country_button_caption = await GetLocalizeTextAsync(LocStringCodes.SIGN_IN_ORG_SEND_COUNTRY_BUTTON, lang);
            var cancel_caption = await GetLocalizeTextAsync(LocStringCodes.CANCEL_BUTTON, lang);


            var cancel = MessageExtensions
                .GetInlineButton(cancel_caption, CommandCodes.SIGN_IN_ORG_CANCEL);

            var markup1 = MessageExtensions.GetInlineMarkup(cancel);

            await this.TelegramClient.SendTextMessageAsync(chatId, message_hello, replyMarkup: markup1);



            return await Task.FromResult(true);
        }
    }
}
