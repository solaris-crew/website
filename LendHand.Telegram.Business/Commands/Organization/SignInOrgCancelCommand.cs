﻿using LendHand.Resources.Adapter;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace LendHand.Telegram.Business.Commands.Organization
{
    public class SignInOrgCancelCommand : CommandBase
    {
        protected IResourcesAdapter resources;

        public SignInOrgCancelCommand(IResourcesAdapter adapter) : base(adapter)
        {
            this.resources = adapter;
        }

        public async override Task<bool> ExecuteAsync(Update update)
        {
            return await this.InvokeNextAsync(update);
        }

        protected override async Task<bool> InvokeNextAsync(Update update)
        {
            var chatId = update.CallbackQuery?.From.Id ?? update.Message.From.Id;
            var lang = update.CallbackQuery?.From.LanguageCode ?? update.Message.From.LanguageCode;

            await this.TelegramClient.DeleteMessageAsync(chatId, update.CallbackQuery.Message.MessageId);

            return await Task.FromResult(true);
        }
    }
}