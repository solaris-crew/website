﻿using LendHand.Resources.Adapter;
using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InlineQueryResults;

namespace LendHand.Telegram.Business.Commands.Organization
{
    public class SignInOrgCountryCommand : CommandBase
    {
        protected IResourcesAdapter resources;

        public SignInOrgCountryCommand(IResourcesAdapter adapter) : base(adapter)
        {
            this.resources = adapter;
        }

        public async override Task<bool> ExecuteAsync(Update update)
        {
            return await this.InvokeNextAsync(update);
        }

        protected override async Task<bool> InvokeNextAsync(Update update)
        {

            return true;
        }
    }
}
