﻿using LendHand.Resources.Adapter;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace LendHand.Telegram.Business.Commands.Organization
{
    public class SignInOrgNameCommand : CommandBase
    {
        protected IResourcesAdapter resources;

        public SignInOrgNameCommand(IResourcesAdapter adapter) : base(adapter)
        {
            this.resources = adapter;
        }

        public async override Task<bool> ExecuteAsync(Update update)
        {
            return await this.InvokeNextAsync(update);
        }

        protected override async Task<bool> InvokeNextAsync(Update update)
        {
            return true;
        }
    }
}
