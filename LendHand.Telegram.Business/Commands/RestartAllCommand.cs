﻿using LendHand.Resources.Adapter;
using LendHand.State.Services;
using LendHand.Telegram.Business.Commands.Extensions;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace LendHand.Telegram.Business.Commands
{
    public class RestartAllCommand : CommandBase
    {

        protected IResourcesAdapter resources;
        protected IStateService stateService;

        public RestartAllCommand(
            IStateService stateService,
            IResourcesAdapter adapter) : base(adapter)
        {
            this.resources = adapter;
            this.stateService = stateService;
        }

        public async override Task<bool> ExecuteAsync(Update update)
        {
            return await this.InvokeNextAsync(update);
        }

        protected override async Task<bool> InvokeNextAsync(Update update)
        {
            var chatId = update.CallbackQuery?.From.Id ?? update.Message.From.Id;
            var lang = update.CallbackQuery?.From.LanguageCode ?? update.Message.From.LanguageCode;

            var state = await this.stateService.Remove(chatId);

            var button = MessageExtensions
                .GetInlineButton("START", CommandCodes.START);

            var markup = MessageExtensions.GetInlineMarkup(button);

            await this.TelegramClient.SendTextMessageAsync(chatId, "Your bot has restarted.", replyMarkup: markup);

            return await Task.FromResult(true);
        }
    }
}
