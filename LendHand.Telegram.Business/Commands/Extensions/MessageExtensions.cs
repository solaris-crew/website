﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace LendHand.Telegram.Business.Commands.Extensions
{
    public static class MessageExtensions
    {
        public static ReplyKeyboardMarkup GetReplyBlock(params string[] texts)
        {
            return new ReplyKeyboardMarkup(GetKeyBoardButtons(texts));
        }

        public static IEnumerable<KeyboardButton> GetKeyBoardButtons(params string[] texts)
        {
            foreach (var ent in texts)
            {
                yield return new KeyboardButton() { Text = ent };
            }
        }

        public static InlineKeyboardMarkup GetInlineMarkup(params InlineKeyboardButton[] inlines)
        {
            return new InlineKeyboardMarkup(inlines);
        }

        public static InlineKeyboardButton GetInlineButton(string text, string callBackText)
        {
            return new InlineKeyboardButton() { Text = text, CallbackData = callBackText };
        }
        public static InlineKeyboardButton GetInlineButtonUrl(string text, string url)
        {
            return new InlineKeyboardButton() { Text = text, Url = url };
        }

        public static InlineKeyboardMarkup GetLinkButton(string text, string url, string callBack)
        {
            var btn = new InlineKeyboardButton()
            {
                Text = text,
                Url = url,
                CallbackData = callBack
            };
            return new InlineKeyboardMarkup(btn);
        }
    }
}
