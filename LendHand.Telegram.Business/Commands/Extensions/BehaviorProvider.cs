﻿using LendHand.Telegram.Business.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendHand.Telegram.Business.Commands.Extensions
{
    public class BehaviorProvider : IBehaviorProvider
    {
        public bool CouldMove(string currentState, string nextState)
        {
            if (behaviors.TryGetValue(currentState, out var model))
            {
                return model.Steps.Any(x => x == nextState);
            }

            return false;
        }

        public string[] GetAvailableNextStates(string currentState)
        {
            if (behaviors.TryGetValue(currentState, out var model))
            {
                return model.Steps;
            }

            return Array.Empty<string>();
        }

        public bool IsStart(string currentState)
        {
            if (behaviors.TryGetValue(currentState, out var model))
            {
                return model.IsStart;
            }

            return false;
        }

        private static Dictionary<string, StateDescription> behaviors = new Dictionary<string, StateDescription>
        {
            { CommandCodes.START,
                new StateDescription(CommandCodes.START, true, CommandCodes.SIGN_IN_VOLUNTEER, CommandCodes.SIGN_IN_NEEDY, CommandCodes.SIGN_IN_ORG) },
            { CommandCodes.SIGN_IN_ORG,
                new StateDescription(CommandCodes.SIGN_IN_ORG, false, CommandCodes.SIGN_IN_ORG_CANCEL,CommandCodes.SIGN_IN_ORG_COUNTRY) },
            
            { CommandCodes.SIGN_IN_ORG_CANCEL,
                new StateDescription(CommandCodes.SIGN_IN_ORG_CANCEL, false, CommandCodes.SIGN_IN_VOLUNTEER, CommandCodes.SIGN_IN_NEEDY, CommandCodes.SIGN_IN_ORG) },
           
            
            { CommandCodes.SIGN_IN_ORG_COUNTRY,
                new StateDescription(CommandCodes.SIGN_IN_ORG_COUNTRY, false, CommandCodes.SIGN_IN_ORG_NAME) },

            { CommandCodes.ERROR, new StateDescription(CommandCodes.ERROR, false, CommandCodes.RESTART_ALL) }
        };

        class StateDescription
        {
            public StateDescription(string name, bool isStart, params string[] steps)
            {
                this.Name = name;
                this.IsStart = isStart;
                this.Steps = steps;
            }

            public string Name { get; set; }
            public bool IsStart { get; set; }
            public string[] Steps { get; set; }

        }
    }
}
