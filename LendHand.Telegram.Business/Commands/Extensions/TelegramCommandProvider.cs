﻿using LendHand.Telegram.Business.Abstraction;
using LendHand.Telegram.Business.Commands.Organization;
using System;
using System.Collections.Generic;

namespace LendHand.Telegram.Business.Commands
{
    public class TelegramCommandProvider : ICommandProvider
    {
        public Type GetCommandByName(string stateCommandName)
        {
            if (_resolver.TryGetValue(stateCommandName, out var type))
                return type;

            return null;
        }

        private static readonly Dictionary<string, Type> _resolver = new Dictionary<string, Type>(
            new Dictionary<string, Type>
            {
                // #md on bot start
                { CommandCodes.START, typeof(StartCommand) },
                { CommandCodes.ERROR, typeof(ErrorCommand) },
                { CommandCodes.RESTART_ALL, typeof(RestartAllCommand) },
                // #md when chose
                { CommandCodes.SIGN_IN_ORG, typeof(SignInOrgCommand) },
                { CommandCodes.SIGN_IN_ORG_CANCEL, typeof(SignInOrgCancelCommand) },
                //{ CommandCodes.SIGN_IN_VOLUNTEER, (long id) =>typeof(VolunteerCommand) },
                //{ CommandCodes.SIGN_IN_NEEDY, (long id) => typeof(NeedyCommand) },


                { CommandCodes.SIGN_IN_ORG_COUNTRY, typeof(SignInOrgCountryCommand) },
                { CommandCodes.SIGN_IN_ORG_NAME, typeof(SignInOrgNameCommand) },

                // #md needy
                // #md when ask for help -> returns categories
                //{ CommandCodes.NEED_HELP, (long id) => typeof(NeedHelpCommand) },
                //// #md when chose category -> returns ask location
                //{ CommandCodes.NEED_HELP_FOOD, (long id) => typeof(NeedHelpFoodCommand) },
                //// #md when location sended -> starts process
                //{ CommandCodes.NEED_HELP_LOCATION, (long id) => typeof(NeedHelpLocationGivenCommand) },

                //{ CommandCodes.LOCATION, (long id) =>
                //    {
                //        var exists = Shit.Shits[id];
                //        if (exists == 1)
                //        {
                //            return typeof(NeedHelpLocationGivenCommand); // Volunteer here
                //        }
                //        else if (exists == 2)
                //        {
                //            return typeof(NeedHelpLocationGivenCommand);
                //        }
                //        else
                //        {
                //            return typeof(NeedHelpLocationGivenCommand); // FUCK YOU, CHEATER
                //        }
                //    }
                //},
                // more  
                // commands here
            });
    }
}
    