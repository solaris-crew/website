﻿namespace LendHand.Telegram.Business.Commands
{
    public static class CommandCodes
    {
        public static readonly string START = "/start";
        public static readonly string ERROR = "/error";
        public static readonly string RESTART_ALL = "/restart_all";
        public static readonly string SIGN_IN_ORG = "/sign_in_org";
        public static readonly string SIGN_IN_ORG_CANCEL = "/sign_in_org_cancel";
        public static readonly string SIGN_IN_VOLUNTEER = "/sign_in_volunteer";
        public static readonly string SIGN_IN_NEEDY = "/sign_in_needy";
        public static readonly string SIGN_IN_ORG_COUNTRY = "/sign_in_org_country";
        public static readonly string SIGN_IN_ORG_NAME = "/sign_in_org_name";


        public static readonly string NEED_HELP = "/need_help";
        public static readonly string NEED_HELP_FOOD = "/need_help_food";
        public static readonly string NEED_HELP_LOCATION = "/need_help_location";

        public static readonly string LOCATION = "/location_handle";

        public static readonly string ACTIVE = "/is_active";
    }

    public static class CodeConstants
    {
        public static class LocStringCodes
        {
            public static readonly string MESSAGE_HELLO = "message_hello";
            public static readonly string MESSAGE_HELLO_1 = "message_hello_1";
            public static readonly string MESSAGE_HELLO_2 = "message_hello_2";
            public static readonly string MESSAGE_HELLO_3 = "message_hello_3";

            public static readonly string POLICY_BUTTON = "button_policy";
            public static readonly string SIGN_VOLUNTEER_BUTTON = "button_sign_in_volunteer";
            public static readonly string SIGN_NEEDY_BUTTON = "button_sign_in_needy";
            public static readonly string SIGN_ORG_BUTTON = "button_sign_in_org";

            public static readonly string HELLO_ORG_MESSAGE = "message_hello_org";
            public static readonly string SIGN_IN_ORG_SEND_COUNTRY_BUTTON = "button_sign_in_org_send_country";
            public static readonly string CANCEL_BUTTON = "cancel";

            public static readonly string NOTIF_MESSAGE = "notif_message";
            public static readonly string ORDER_TYPE = "change_type";
            public static readonly string HELP_CATEGORY = "help_category";
            public static readonly string FOOD = "food_category";
            public static readonly string MEDICINES = "medicines_category";
            public static readonly string OTHER_DELIVERY = "other_category";
            public static readonly string GET_LOCATION_BUTTON = "get_location_button";
            public static readonly string GET_LOCATION_MESSAGE = "get_location_message";
            public static readonly string ACTIVE_BUTTON = "active_vol_button";
            public static readonly string ACTIVE_MESSAGE = "active_vol_message";



            public static readonly string NEED_HELP_MESSAGE = "message_help";
            public static readonly string NEED_HELP_BUTTON = "button_help";

            public static readonly string NEED_HELP_TYPE_MESSAGE = "message_help_type";
            public static readonly string NEED_HELP_TYPE_FOOD_BUTTON = "button_help_type_food";
            public static readonly string NEED_HELP_TYPE_MED_BUTTON = "button_help_type_medecine";
            public static readonly string NEED_HELP_TYPE_OTHER_BUTTON = "button_help_type_other";

            public static readonly string NEED_HELP_LOCATION_MESSAGE = "message_help_location";
            public static readonly string NEED_HELP_LOCATION_BUTTON = "button_help_location";


        }
    }

    public static class Smiles
    {
        public static readonly string SOS = "%F0%9F%86%98 {0}";
        public static readonly string VOLUNTEER = "%F0%9F%91%8B {0}";
    }
}
