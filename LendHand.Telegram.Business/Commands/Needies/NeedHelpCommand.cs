﻿using LendHand.Resources.Adapter;
using LendHand.Telegram.Business.Commands.Extensions;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using static LendHand.Telegram.Business.Commands.CodeConstants;

namespace LendHand.Telegram.Business.Commands
{
    public class NeedHelpCommand : CommandBase
    {
        public NeedHelpCommand( 
            IResourcesAdapter adapter) : base(adapter)
        {

        }

        public async override Task<bool> ExecuteAsync(Update update)
        {
            return await this.InvokeNextAsync(update);
        }

        protected override async Task<bool> InvokeNextAsync(Update update)
        {
            var chatId = update.CallbackQuery.Message.Chat.Id;
            var lang = update.CallbackQuery.From.LanguageCode;

            var prm = await GetLocalizeTextAsync(LocStringCodes.NEED_HELP_TYPE_MESSAGE, lang);
            var food_btn_cpt = await GetLocalizeTextAsync(LocStringCodes.NEED_HELP_TYPE_FOOD_BUTTON, lang);

            // #md create categories button
            var food_button = MessageExtensions
                .GetInlineButton(food_btn_cpt, CommandCodes.NEED_HELP_FOOD);

            var markup = MessageExtensions.GetInlineMarkup(food_button);
            await this.TelegramClient.SendTextMessageAsync(chatId, prm, replyMarkup: markup);

            return true;
        }
    }
}
