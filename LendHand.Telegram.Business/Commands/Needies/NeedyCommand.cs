﻿using LendHand.Contracts.Dto.Bot;
using LendHand.Resources.Adapter;
using LendHand.Telegram.Business.Commands.Extensions;
using LendHand.Telegram.Business.Services.Interfaces;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using static LendHand.Telegram.Business.Commands.CodeConstants;

namespace LendHand.Telegram.Business.Commands
{
    public class NeedyCommand : CommandBase
    {
        protected IResourcesAdapter resources;
        protected INeedyService needlyService;

        public NeedyCommand(
            IResourcesAdapter adapter,
            INeedyService needlyService) : base(adapter)
        {
            this.resources = adapter;
            this.needlyService = needlyService;
        }

        public async override Task<bool> ExecuteAsync(Update update)
        {
            await needlyService.Add(new NeedyDto
            {
                TelegramChatId = update.CallbackQuery.Message.From.Id,
                IsPoliciesAccepted = true,
                LanguageCode = update.CallbackQuery.Message.From.LanguageCode
            });
            return await this.InvokeNextAsync(update);
        }


        protected override async Task<bool> InvokeNextAsync(Update update)
        {
            var chatId = update.CallbackQuery.Message.Chat.Id;
            var lang = update.CallbackQuery.From.LanguageCode;

            var need_help_message = await GetLocalizeTextAsync(LocStringCodes.NEED_HELP_MESSAGE, lang);
            var need_help_btn_caption = await GetLocalizeTextAsync(LocStringCodes.NEED_HELP_BUTTON, lang);
            var need_help_btn = MessageExtensions.GetInlineButton(need_help_btn_caption, CommandCodes.NEED_HELP);

            var markup = MessageExtensions.GetInlineMarkup(need_help_btn);
            await this.TelegramClient.SendTextMessageAsync(chatId, need_help_message, replyMarkup: markup);

            return await Task.FromResult(true);
        }
    }
}
