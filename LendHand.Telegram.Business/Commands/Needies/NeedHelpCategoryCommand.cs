﻿using LendHand.Resources.Adapter;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace LendHand.Telegram.Business.Commands.Needies
{
    public abstract class NeedHelpCategoryCommand : CommandBase
    {
        public abstract int CategoryId { get; }

        public NeedHelpCategoryCommand(IResourcesAdapter adapter)
            : base(adapter)
        {

        }

        public override async Task<bool> ExecuteAsync(Update update)
        {
            return await this.InvokeNextAsync(update);
        }

        protected override async Task<bool> InvokeNextAsync(Update update)
        {
            var chatId = update.CallbackQuery.Message.Chat.Id;
            var lang = update.CallbackQuery.From.LanguageCode;
            var message = await GetLocalizeTextAsync(CodeConstants.LocStringCodes.NEED_HELP_LOCATION_MESSAGE, lang);
            var loc_button_caption = await GetLocalizeTextAsync(CodeConstants.LocStringCodes.NEED_HELP_LOCATION_BUTTON, lang);
            var loc_button = KeyboardButton.WithRequestLocation(loc_button_caption);
            
            var markup = new ReplyKeyboardMarkup(loc_button);
            await this.TelegramClient.SendTextMessageAsync(chatId, message, replyMarkup: markup);

            return true;
        }
    }
}
