﻿using LendHand.Resources.Adapter;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace LendHand.Telegram.Business.Commands.Needies
{
    public class NeedHelpLocationGivenCommand : CommandBase
    {
        public NeedHelpLocationGivenCommand(
            IResourcesAdapter adapter) : base(adapter)
        {

        }

        public override async Task<bool> ExecuteAsync(Update update)
        {
            return await this.InvokeNextAsync(update);
        }

        protected override async Task<bool> InvokeNextAsync(Update update)
        {
            // statr search and notify process
            return true;
        }
    }
}
