﻿using LendHand.Resources.Adapter;

namespace LendHand.Telegram.Business.Commands.Needies
{
    public class NeedHelpFoodCommand : NeedHelpCategoryCommand
    {
        public NeedHelpFoodCommand(IResourcesAdapter adapter) : base(adapter) { }

        public override int CategoryId => 1;
    }
}
