﻿using AutoMapper;
using LendHand.Contracts.Dto.Bot;
using LendHand.Telegram.Business.Services.Interfaces;
using LendHand.Telegram.Data.Entities;
using LendHand.Telegram.Data.Repository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendHand.Telegram.Business.Services
{
    public class InvocationService : IInvocationService
    {
        private readonly IVolunteerRepository volunteerRepository;
        private readonly IInvocationRepository invocationRepository;
        private readonly IMapper mapper;

        public InvocationService(
            IVolunteerRepository volunteerRepository,
            IInvocationRepository invocationRepository,
            IMapper mapper)
        {
            this.volunteerRepository = volunteerRepository;
            this.invocationRepository = invocationRepository;
            this.mapper = mapper;
        }

        public async Task<int> Add(AddInvocationDto invocation)
        {
            var entity = this.mapper.Map<Invocation>(invocation);

            entity.StatusId = (int)Enums.InvocationStatus.Open;
            await this.invocationRepository.Add(entity);

            return entity.Id;
        }
        
        public async Task<InvocationDto> GetNeedyCurrent(int needyId)
        {
            var entity = await this.invocationRepository
                .Query(x => x.NeedyId == needyId && x.StatusId != (int)Enums.InvocationStatus.Done)
                .FirstOrDefaultAsync();

            return this.mapper.Map<InvocationDto>(entity);
        }

        public async Task<List<InvocationDto>> GetNeedyHistory(int needyId)
        {
            var list = await this.invocationRepository.Get(x => x.NeedyId == needyId);
            return this.mapper.Map<List<InvocationDto>>(list);
        }

        public async Task<InvocationDto> GetVolunteerCurrent(int volunteerId)
        {
            var entity = await this.invocationRepository
                .Query(x => x.NeedyId == volunteerId && x.StatusId == (int)Enums.InvocationStatus.Progress)
                .FirstOrDefaultAsync();

            return this.mapper.Map<InvocationDto>(entity);
        }

        public async Task<List<InvocationDto>> GetVolunteerHistory(int volunteerId)
        {
            var list = await this.invocationRepository.Get(x => x.VolunteerId == volunteerId);
            return this.mapper.Map<List<InvocationDto>>(list);
        }

        public async Task<int> Reopen(int id)
        {
            var exists = await this.invocationRepository.GetById(id);
            if (exists == null)
            {
                // #todo throw new LendHandNotFoundExeption<Invocation>("id", id);
                return -1;
            }

            if (exists.StatusId != (int)Enums.InvocationStatus.Progress)
            {
                // #todo throw new LendHandInvalidDataExeption("You could reopen only a started invocation.");
                return -2;
            }

            exists.StatusId = (int)Enums.InvocationStatus.Open;
            exists.VolunteerId = 0;
            exists.Volontaire = null;

            return exists.Id;
        }

        public async Task<int> Start(int id, long volunteerTelegramId)
        {
            var exists = await this.invocationRepository.GetById(id);
            if (exists == null)
            {
                // #todo throw new LendHandNotFoundExeption<Invocation>("id", id);
                return -1;
            }

            if (exists.StatusId != (int)Enums.InvocationStatus.Open)
            {
                // #todo throw new LendHandInvalidDataExeption("You could start only an open invocation.");
                return -2;
            }

            var volunteer = await this.volunteerRepository.GetByChat(volunteerTelegramId);
            if (volunteer == null)
            {
                // #todo throw new LendHandNotFoundExeption<Volunteer>("TelegramChatId", volunteerTelegramId);
                return -3;
            }

            exists.StatusId = (int)Enums.InvocationStatus.Progress;
            exists.VolunteerId = volunteer.Id;
            exists.Volontaire = volunteer;

            return exists.Id;
        }

        public async Task<int> Done(int id)
        {
            var exists = await this.invocationRepository.GetById(id);
            if (exists == null)
            {
                // #todo throw new LendHandNotFoundExeption<Invocation>("id", id);
                return -1;
            }

            if (exists.StatusId != (int)Enums.InvocationStatus.Progress)
            {
                // #todo throw new LendHandInvalidDataExeption("You could finsh only an started invocation.");
                return -2;
            }

            exists.StatusId = (int)Enums.InvocationStatus.Done;

            return exists.Id;
        }
    }
}
