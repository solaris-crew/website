﻿using AutoMapper;
using LendHand.Contracts.Dto.Bot;
using LendHand.Telegram.Business.Services.Interfaces;
using LendHand.Telegram.Data.Entities;
using LendHand.Telegram.Data.Repository;
using System.Threading.Tasks;

namespace LendHand.Telegram.Business.Services
{
    public class NeedyService : INeedyService
    {
        private readonly INeedyRepository needyRepository;
        private readonly IMapper mapper;

        public NeedyService(
            INeedyRepository needyRepository,
            IMapper mapper)
        {
            this.needyRepository = needyRepository;
            this.mapper = mapper;
        }

        public async Task<int> Add(NeedyDto needy)
        {
            var exists = await this.needyRepository.GetByChat(needy.TelegramChatId);
            if (exists != null)
            {
                return await this.UpdateIternal(needy, exists);
            }

            var entity = this.mapper.Map<Needy>(needy);
            await this.needyRepository.Add(entity);
            return entity.Id;
        }

        public async Task<NeedyDto> Delete(int id)
        {
            var exists = await this.needyRepository.GetById(id);
            if (exists == null)
            {
                return null;
            }

            await this.needyRepository.Remove(exists);

            return this.mapper.Map<NeedyDto>(exists);
        }

        public async Task<NeedyDto> Get(long chatId)
        {
            var entity = await this.needyRepository.GetByChat(chatId);
            return this.mapper.Map<NeedyDto>(entity);
        }

        public async Task<int> Update(NeedyDto needy, int id)
        {
            var exists = await this.needyRepository.GetById(id);
            if (exists == null)
            {
                return -1;
            }

            return await this.UpdateIternal(needy, exists);
        }

        private async Task<int> UpdateIternal(NeedyDto needy, Needy exists)
        {
            this.mapper.Map(needy, exists);
            await this.needyRepository.Update(exists);

            return exists.Id;
        }
    }
}
