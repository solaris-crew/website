﻿namespace LendHand.Telegram.Business.Services.Enums
{
    public enum InvocationStatus
    {
        Open = 1,
        Progress = 2,
        Done =3
    }
}
