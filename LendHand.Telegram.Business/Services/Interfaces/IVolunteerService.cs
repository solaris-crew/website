﻿using LendHand.Contracts.Dto.Bot;
using System.Threading.Tasks;

namespace LendHand.Telegram.Business.Services.Interfaces
{
    public interface IVolunteerService
    {
        Task<int> Add(VolunteerDto volunteer);
        Task<int> Update(VolunteerDto volunteer, int id);
        Task<VolunteerDto> Get(long chatId);
        Task<VolunteerDto> Delete(int id);
    }
}
