﻿using LendHand.Contracts.Dto.Bot;
using System.Threading.Tasks;

namespace LendHand.Telegram.Business.Services.Interfaces
{
    public interface INeedyService
    {
        Task<int> Add(NeedyDto needy);
        Task<int> Update(NeedyDto needy, int id);
        Task<NeedyDto> Get(long chatId);
        Task<NeedyDto> Delete(int id);
    }
}
