﻿using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace LendHand.Telegram.Business.Services.Interfaces
{
    public interface ITelegramProcessor
    {
        Task Update(Update update);
    }
}
