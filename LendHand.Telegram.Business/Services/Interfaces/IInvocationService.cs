﻿using LendHand.Contracts.Dto.Bot;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendHand.Telegram.Business.Services.Interfaces
{
    public interface IInvocationService
    {
        Task<int> Add(AddInvocationDto invocation);
        Task<int> Start(int id, long volunteerTelegramId);
        Task<int> Reopen(int id);
        Task<int> Done(int id);

        Task<List<InvocationDto>> GetVolunteerHistory(int volunteerId);
        Task<List<InvocationDto>> GetNeedyHistory(int needyId);
        Task<InvocationDto> GetVolunteerCurrent(int volunteerId);
        Task<InvocationDto> GetNeedyCurrent(int needyId);
    }
}
