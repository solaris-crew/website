﻿using LendHand.State.Services;
using LendHand.Telegram.Business.Abstraction;
using LendHand.Telegram.Business.Commands;
using LendHand.Telegram.Business.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace LendHand.Telegram.Business.Services
{
    public class TelegramProcessor : ITelegramProcessor
    {
        private readonly IStateService stateService;
        private readonly ILogger<TelegramProcessor> logger;
        private readonly ICommandProvider commandProvider;
        private readonly IBehaviorProvider behaviorProvider;
        private readonly IServiceProvider provider;

        private State.Entities.State state;

        public TelegramProcessor(
            IStateService stateService,
            ILogger<TelegramProcessor> logger,
            ICommandProvider commandProvider,
            IBehaviorProvider behaviorProvider,
            IServiceProvider provider)
        {
            this.stateService = stateService;
            this.logger = logger;
            this.commandProvider = commandProvider;
            this.behaviorProvider = behaviorProvider;
            this.provider = provider;
        }

        public async Task Update(Update update)
        {
            logger.LogInformation("Update method called.");
            await this.SwitchCommand(update);
            logger.LogInformation("Update method ended.");
        }

        private async Task SwitchCommand(Update update)
        {
            var (id, pattern) = this.GetInfo(update);
            pattern = await this.CompareInfoByState(id, pattern);

            var command = this.CreateCommand(pattern);
            if (command != null)
                await command.ExecuteAsync(update);
        }

        private (long id, string pattern) GetInfo(Update update)
        {
            string pattern;
            long id;
            if (update.Type == UpdateType.Message)
            {
                id = update.Message.From.Id;
                if (update.Message.Type == MessageType.Text)
                {
                    pattern = update.Message.Text;
                }
                else if (update.Message.Type == MessageType.Location)
                {
                    pattern = "/location_handle";
                }
                else
                {
                    pattern = "/error";
                }
            }
            else if (update.Type == UpdateType.CallbackQuery)
            {
                id = update.CallbackQuery.From.Id;
                pattern = update.CallbackQuery.Data;
            }
            else
            {
                pattern = "/error";
                id = -1;
            }

            return (id, pattern);
        }

        private async Task<string> CompareInfoByState(long id, string messagePattern)
        {
            if (messagePattern == CommandCodes.ERROR || messagePattern == CommandCodes.RESTART_ALL)
            {
                return messagePattern;
            }

            this.state = await this.stateService.Get(id);
            if (this.state == null)
            {
                if (this.behaviorProvider.IsStart(messagePattern))
                {
                    this.state = new State.Entities.State
                    {
                        TelegramId = id,
                        StateCode = messagePattern
                    };
                    await this.stateService.AddState(this.state);
                    return messagePattern;
                }
                
                return CommandCodes.ERROR;
            }

            if (this.behaviorProvider.CouldMove(this.state.StateCode, messagePattern))
            {
                this.state.StateCode = messagePattern;
                await this.stateService.UpdateState(this.state);
                return messagePattern;
            }

            return CommandCodes.ERROR;
        }

        private CommandBase CreateCommand(string pattern)
        {
            var type = this.commandProvider.GetCommandByName(pattern);
            return type == null ? null : this.provider.GetService(type) as CommandBase;
        }
    }
}
