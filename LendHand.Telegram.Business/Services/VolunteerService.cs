﻿using AutoMapper;
using LendHand.Contracts.Dto.Bot;
using LendHand.Telegram.Business.Services.Interfaces;
using LendHand.Telegram.Data.Entities;
using LendHand.Telegram.Data.Repository;
using System.Threading.Tasks;

namespace LendHand.Telegram.Business.Services
{
    public class VolunteerService : IVolunteerService
    {
        private readonly IVolunteerRepository volunteerRepository;
        private readonly IMapper mapper;

        public VolunteerService(
            IVolunteerRepository volunteerRepository,
            IMapper mapper)
        {
            this.volunteerRepository = volunteerRepository;
            this.mapper = mapper;
        }

        public async Task<int> Add(VolunteerDto volunteer)
        {
            var exists = await this.volunteerRepository.GetByChat(volunteer.TelegramChatId);
            if (exists != null)
            {
                return await this.UpdateIternal(volunteer, exists);
            }

            var entity = this.mapper.Map<Volunteer>(volunteer);
            await this.volunteerRepository.Add(entity);
            return entity.Id;
        }

        public async Task<VolunteerDto> Delete(int id)
        {
            var exists = await this.volunteerRepository.GetById(id);
            if (exists == null)
            {
                return null;
            }

            await this.volunteerRepository.Remove(exists);

            return this.mapper.Map<VolunteerDto>(exists);
        }

        public async Task<VolunteerDto> Get(long chatId)
        {
            var entity = await this.volunteerRepository.GetByChat(chatId);
            return this.mapper.Map<VolunteerDto>(entity);
        }

        public async Task<int> Update(VolunteerDto volunteer, int id)
        {
            var exists = await this.volunteerRepository.GetById(id);
            if (exists == null)
            {
                return -1;
            }

            return await this.UpdateIternal(volunteer, exists);
        }


        private async Task<int> UpdateIternal(VolunteerDto volunteer, Volunteer exists)
        {
            this.mapper.Map(volunteer, exists);
            await this.volunteerRepository.Update(exists);

            return exists.Id;
        }
    }
}
