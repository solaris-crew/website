﻿using System;

namespace LendHand.Telegram.Business.Abstraction
{
    public interface ICommandProvider
    {
        Type GetCommandByName(string stateCommandName);
    }
}
