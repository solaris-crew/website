﻿namespace LendHand.Telegram.Business.Abstraction
{
    public interface IBehaviorProvider
    {
        bool CouldMove(string currentState, string nextState);
        bool IsStart(string currentState);
        string[] GetAvailableNextStates(string currenState);
    }
}
