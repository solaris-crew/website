# Lend-a-Hand web app

Lenh-a-Hand web app

Lend-a-Hand is a tool for communication between those who need urgent help and those who can lend a hand. 

The prototype of our solution is based on the Telegram messaging platform. On the one hand, the bot allows people to submit a request for help without downloading apps or visiting other sites, but simply by clicking a few buttons in their messenger. 

On the other, the system is also made for organizations’ members who can choose a convenient time for themselves to receive requests for help.