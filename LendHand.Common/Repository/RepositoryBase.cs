﻿using LendHand.Common.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LendHand.Common.Repository
{
    public class RepositoryBase<TEntity> : IRepository<TEntity>, IDisposable
        where TEntity : class, IEntity
    {
        private bool disposed = false;
        protected readonly DbContext context;

        protected DbSet<TEntity> Set => this.context.Set<TEntity>();

        public RepositoryBase(DbContext context)
        {
            this.context = context;
        }

        public async Task Add(TEntity entity)
        {
            this.SetCreatedOn(entity);
            this.SetUpdatedOn(entity);
            await this.Set.AddAsync(entity);
            await this.Save();
        }

        public async Task AddRange(bool save = true, params TEntity[] entities)
        {
            foreach(var entity in entities)
            {
                this.SetCreatedOn(entity);
                this.SetUpdatedOn(entity);
            }

            await this.Set.AddRangeAsync(entities);
            if (save)
                await this.Save();
        }

        public async Task Remove(TEntity entity)
        {
            this.Set.Remove(entity);
            await this.Save();
        }

        public async Task Remove(int id)
        {
            // todo if id < 0 throw exeption
            var entry = await this.GetById(id);
            // todo if entry is null throw exeption
            this.Set.Remove(entry);
            await this.Save();
        }

        public async Task RemoveRange(bool save = true, params TEntity[] entities)
        {
            this.Set.RemoveRange(entities);
            if (save)
                await this.Save();
        }

        public async Task<int> Save()
        {
            return await this.context.SaveChangesAsync();
        }

        public async Task Update(TEntity entity)
        {
            this.SetUpdatedOn(entity);
            this.Set.Update(entity);
            await this.Save();
        }

        public async Task UpdateRange(bool save = true, params TEntity[] entities)
        {
            foreach (var entity in entities)
            {
                this.SetUpdatedOn(entity);
            }

            this.Set.UpdateRange(entities);
            if (save)
                await this.Save();
        }

        public async Task<List<TEntity>> Get(Expression<Func<TEntity, bool>> filter)
        {
            return await this.Query(filter).ToListAsync();
        }

        IQueryable<TEntity> IRepository<TEntity>.Query()
        {
            return this.Set;
        }

        public async Task<TEntity> GetById(int id)
        {
            return await this.Set.FindAsync(id);
        }

        public IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter)
        {
            return this.Set.Where(filter);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void SetCreatedOn(TEntity entity)
        {
            entity.CreatedOn = DateTime.UtcNow;
        }

        private void SetUpdatedOn(TEntity entity)
        {
            entity.UpdatedOn = DateTime.UtcNow;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
                return;

            if (disposing)
            {
                this.context?.Dispose();
            }

            this.disposed = true;
        }

        ~RepositoryBase()
        {
            this.Dispose(false);
        }
    }
}
