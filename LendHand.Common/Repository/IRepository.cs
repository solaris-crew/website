﻿using LendHand.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LendHand.Common.Repository
{
    public interface IRepository<TEntity>
        where TEntity : class, IEntity
    {
        Task<List<TEntity>> Get(Expression<Func<TEntity, bool>> filter);
        IQueryable<TEntity> Query();
        IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> filter);
        Task<TEntity> GetById(int id);

        Task Add(TEntity entity);
        Task Update(TEntity entity);
        Task Remove(TEntity entity);
        Task Remove(int id);

        Task AddRange(bool save = true, params TEntity[] entities);
        Task UpdateRange(bool save = true, params TEntity[] entities);
        Task RemoveRange(bool save = true, params TEntity[] entities);

        Task<int> Save();
    }
}
