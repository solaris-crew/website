﻿using LendHand.Common.Data;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendHand.Common.Extensions
{
    public static class CollectionExtensions
    {
        public static string ParseToString<TEntity>(this List<TEntity> entities)
            where TEntity : class
        {
            return JsonConvert.SerializeObject(entities);
        }
    }
}
