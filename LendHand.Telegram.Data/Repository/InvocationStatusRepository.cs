﻿using LendHand.Common.Repository;
using LendHand.Telegram.Data.Entities;

namespace LendHand.Telegram.Data.Repository
{
    public class InvocationStatusRepository : RepositoryBase<InvocationStatus>
    {
        public InvocationStatusRepository(TelegramDbContext context)
            : base(context) { }
    }
}
