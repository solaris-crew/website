﻿using LendHand.Common.Repository;
using LendHand.Telegram.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace LendHand.Telegram.Data.Repository
{
    public class VolunteerRepository : RepositoryBase<Volunteer>, IVolunteerRepository
    {
        public VolunteerRepository(TelegramDbContext context)
            : base(context) { }

        public async Task<Volunteer> GetByChat(long chatId)
        {
            return await this
                .Query(x => x.TelegramChatId == chatId)
                .FirstOrDefaultAsync();
        }
    }
}
