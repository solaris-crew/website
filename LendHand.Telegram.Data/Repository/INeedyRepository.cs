﻿using LendHand.Common.Repository;
using LendHand.Telegram.Data.Entities;
using System.Threading.Tasks;

namespace LendHand.Telegram.Data.Repository
{
    public interface INeedyRepository : IRepository<Needy>
    {
        Task<Needy> GetByChat(long chatId);
    }
}
