﻿using LendHand.Common.Repository;
using LendHand.Telegram.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace LendHand.Telegram.Data.Repository
{
    public class NeedyRepository : RepositoryBase<Needy>, INeedyRepository
    {
        public NeedyRepository(TelegramDbContext context)
            : base(context) { }

        public async Task<Needy> GetByChat(long chatId)
        {
            return await this
                .Query(x => x.TelegramChatId == chatId)
                .FirstOrDefaultAsync();
        }
    }
}
