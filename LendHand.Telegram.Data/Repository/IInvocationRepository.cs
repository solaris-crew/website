﻿using LendHand.Common.Repository;
using LendHand.Telegram.Data.Entities;

namespace LendHand.Telegram.Data.Repository
{
    public interface IInvocationRepository : IRepository<Invocation>
    {
    }
}
