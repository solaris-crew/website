﻿using LendHand.Common.Repository;
using LendHand.Telegram.Data.Entities;

namespace LendHand.Telegram.Data.Repository
{
    public class InvocationRepository : RepositoryBase<Invocation>, IInvocationRepository
    {
        public InvocationRepository(TelegramDbContext context)
            : base(context) { }


    }
}
