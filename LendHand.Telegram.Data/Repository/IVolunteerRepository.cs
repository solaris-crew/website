﻿using LendHand.Common.Repository;
using LendHand.Telegram.Data.Entities;
using System.Threading.Tasks;

namespace LendHand.Telegram.Data.Repository
{
    public interface IVolunteerRepository : IRepository<Volunteer>
    {
        Task<Volunteer> GetByChat(long chatId);
    }
}
