﻿using LendHand.Common.Data;

namespace LendHand.Telegram.Data.Entities
{
    public class Readiness : Entity
    {
        public long Time { get; set; }
        public int VolontaireId { get; set; }
        public int NeedyId { get; set; }
        public int CountryId { get; set; }
        public int CityId { get; set; }
        public int DisctrictId { get; set; }

        public Volunteer Volontaire { get; set; }
        public Needy Needy { get; set; }
        public Country Country { get; set; }
        public City City { get; set; }
        public District Disctrict { get; set; }
    }
}
