﻿using LendHand.Common.Data;

namespace LendHand.Telegram.Data.Entities
{
    public abstract class User : Entity
    {
        public string Name { get; set; }
        public int TypeId { get; set; }
        public bool IsPoliciesAccepted { get; set; }
        public long TelegramChatId { get; set; }
        public string LanguageCode { get; set; }

        public UserType Type { get; set; }
    }
}
