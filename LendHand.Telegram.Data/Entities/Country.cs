﻿using LendHand.Common.Data;
using System.Collections.Generic;

namespace LendHand.Telegram.Data.Entities
{
    public class Country : Entity
    {
        public string Name { get; set; }

        public List<City> Cities { get; set; }
    }
}