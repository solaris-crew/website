﻿using LendHand.Common.Data;

namespace LendHand.Telegram.Data.Entities
{
    public class UserType : Entity
    {
        public string Name { get; set; }
    }
}
