﻿using LendHand.Common.Data;

namespace LendHand.Telegram.Data.Entities
{
    public class Invocation : Entity
    {
        public int CountryId { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public int HelpTypeId { get; set; }
        public int VolunteerId { get; set; }
        public int NeedyId { get; set; }
        public int StatusId { get; set; }

        public Country Country { get; set; }
        public City City { get; set; }
        public District District { get; set; }
        public HelpType HelpType { get; set; }
        public Volunteer Volontaire { get; set; }
        public Needy Needy { get; set; }
        public InvocationStatus Status { get; set; }
    }
}
