﻿using LendHand.Common.Data;

namespace LendHand.Telegram.Data.Entities
{
    public class InvocationStatus : Entity
    {
        public string Name { get; set; }
    }
}