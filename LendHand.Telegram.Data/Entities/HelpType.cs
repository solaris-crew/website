﻿using LendHand.Common.Data;

namespace LendHand.Telegram.Data.Entities
{
    public class HelpType : Entity
    {
        public string Name { get; set; }
    }
}