﻿using LendHand.Common.Data;
using System.Collections.Generic;

namespace LendHand.Telegram.Data.Entities
{
    public class City : Entity
    {
        public string Name { get; set; }
        public int CountryId { get; set; }

        public Country Country { get; set; }
        public List<District> Disctricts { get; set; }
    }
}