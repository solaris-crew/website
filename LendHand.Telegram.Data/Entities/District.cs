﻿using LendHand.Common.Data;

namespace LendHand.Telegram.Data.Entities
{
    public class District : Entity
    {
        public string Name { get; set; }
        public int CityId { get; set; }

        public City City { get; set; }
    }
}