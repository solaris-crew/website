﻿using LendHand.Telegram.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace LendHand.Telegram.Data
{
    /// <summary>
    /// Main crm database context
    /// </summary>
    public class TelegramDbContext : DbContext
    {
        public TelegramDbContext(
            [NotNull]DbContextOptions<TelegramDbContext> options)
            : base(options) { }

        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserType> UserType { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<District> District { get; set; }
        public virtual DbSet<HelpType> HelpType { get; set; }
        public virtual DbSet<Invocation> Invocation { get; set; }
        public virtual DbSet<InvocationStatus> InvocationStatus { get; set; }
        public virtual DbSet<Readiness> Readiness { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            this.AddUserTypeData(modelBuilder);
            this.AddInvocationStatusData(modelBuilder);
            this.AddHelpTypeData(modelBuilder);

            modelBuilder.Entity<User>()
                .HasDiscriminator(x => x.TypeId)
                .HasValue<Volunteer>(Constants.UserType.VOLONTAIRE)
                .HasValue<Needy>(Constants.UserType.NEEDY);

            modelBuilder.Entity<Volunteer>();
            modelBuilder.Entity<Needy>();
        }

        private void AddUserTypeData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserType>()
                .HasData(new UserType { Id = Constants.UserType.VOLONTAIRE, Name = "Volunteer" });
            modelBuilder.Entity<UserType>()
                .HasData(new UserType { Id = Constants.UserType.NEEDY, Name = "Needy" });
        }

        private void AddInvocationStatusData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<InvocationStatus>()
                .HasData(new InvocationStatus { Id = 1, Name = "Open" });
            modelBuilder.Entity<InvocationStatus>()
                .HasData(new InvocationStatus { Id = 2, Name = "Progress" });
            modelBuilder.Entity<InvocationStatus>()
                .HasData(new InvocationStatus { Id = 3, Name = "Done" });
        }

        private void AddHelpTypeData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<HelpType>()
                .HasData(new HelpType { Id = 1, Name = "Food" });
            modelBuilder.Entity<HelpType>()
                .HasData(new HelpType { Id = 2, Name = "Medecine" });
            modelBuilder.Entity<HelpType>()
                .HasData(new HelpType { Id = 3, Name = "Other" });
        }
    }
}
