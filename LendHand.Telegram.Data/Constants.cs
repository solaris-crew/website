﻿namespace LendHand.Telegram.Data
{
    public static class Constants
    {
        public static class UserType
        {
            public static readonly int VOLONTAIRE = 1;
            public static readonly int NEEDY = 2;
        }
    }
}
