﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LendHand.Contracts.Dto.Resource
{
    public class ResourceDto
    {
        [Required]
        public string Code { get; set; }
        [Required]
        public string Value { get; set; }
        [Required]
        [DefaultValue(1)]
        public int LanguageId { get; set; }
        [Required]
        [DefaultValue(1)]
        public int TargetId { get; set; }
    }
}
