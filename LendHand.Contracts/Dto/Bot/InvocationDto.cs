﻿namespace LendHand.Contracts.Dto.Bot
{
    public class InvocationDto
    {
        public int VolunteerId { get; set; }
        public int NeedyId { get; set; }
        public int HelpTypeId { get; set; }
        public int Id { get; private set; }
    }
}
