﻿namespace LendHand.Contracts.Dto.Bot
{
    public class AddInvocationDto
    {
        public int NeedyId { get; set; }
        public int HelpTypeId { get; set; }
    }
}
