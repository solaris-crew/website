﻿namespace LendHand.Contracts.Dto.Bot
{
    public class VolunteerDto
    {
        public string Name { get; set; }
        public long TelegramChatId { get; set; }
        public string LanguageCode { get; set; }
        public bool IsPoliciesAccepted { get; set; }
        public int Id { get; private set; }
    }
}
