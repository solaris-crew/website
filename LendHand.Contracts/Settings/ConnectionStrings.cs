﻿namespace LendHand.Contracts.Settings
{
    public class ConnectionStrings
    {
        public string Db0 { get; set; }
        public string Db1 { get; set; }
        public string Db2 { get; set; }
        public string LogsDb { get; set; }
    }
}
