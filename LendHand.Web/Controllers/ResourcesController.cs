﻿using LendHand.Common.Extensions;
using LendHand.Contracts.Dto.Resource;
using LendHand.Resources.Adapter;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LendHand.Web.Controllers
{
    /// <summary>
    /// Controller for resources managment.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ResourcesController : ControllerBase
    {
        private readonly IResourcesAdapter resourcesAdapter;

        /// <summary>
        /// Creates new instance of <see cref="ResourcesController"/>
        /// </summary>
        /// <param name="resourcesAdapter"></param>
        public ResourcesController(IResourcesAdapter resourcesAdapter)
        {
            this.resourcesAdapter = resourcesAdapter;
        }

        /// <summary>
        /// Allows to get resource by code for language.
        /// </summary>
        /// <param name="languageId">Language id, PK in db</param>
        /// <param name="code">Searching resource code.</param>
        [HttpGet("{languageId}/{code}")]
        public async Task<IActionResult> Get(int languageId, string code)
        {
            var resource = await this.resourcesAdapter.GetResource(code, languageId);
            if (resource == null)
            {
                return this.BadRequest("Invalid data.");
            }

            return this.Content(resource.ToString());
        }

        /// <summary>
        /// Allows to get full resource by its id.
        /// </summary>
        /// <param name="id">PK of resource.</param>
        [HttpGet("by/id/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var resource = await this.resourcesAdapter.GetResource(id);
            if (resource == null)
            {
                return this.BadRequest("Invalid data.");
            }

            return this.Content(resource.ToString());
        }

        /// <summary>
        /// Allows to get full resource by its code.
        /// </summary>
        /// <param name="code">Code of resource.</param>
        [HttpGet("by/code/{code}")]
        public async Task<IActionResult> GetResource(string code)
        {
            var resource = await this.resourcesAdapter.GetResource(code);
            if (resource == null)
            {
                return this.BadRequest("Invalid data.");
            }

            return this.Content(resource.ToString());
        }

        /// <summary>
        /// Allows to get all resources for given target.
        /// </summary>
        /// <param name="target">Target id - 1 for wesites, 2 for telegram.</param>
        [HttpGet("all/{target}")]
        public async Task<IActionResult> GetAll(int target)
        {
            var list = await this.resourcesAdapter.GetAll(target);
            return this.Content(list.ParseToString());
        }

        /// <summary>
        /// Allows to get all resources for given target with given language.
        /// </summary>
        /// <param name="languageId">Language id.</param>
        /// <param name="target">Target id - 1 for wesites, 2 for telegram.</param>
        [HttpGet("all/{languageId}/{target}")]
        public async Task<IActionResult> GetAll(int languageId, int target = 1)
        {
            var list = await this.resourcesAdapter.GetAll(languageId, target);
            return this.Content(list.ParseToString());
        }

        /// <summary>
        /// Allows to add new resource.
        /// </summary>
        /// <param name="resource">Resource model.</param>
        [HttpPost]
        public async Task<IActionResult> Add(ResourceDto resource)
        {
            var id = await this.resourcesAdapter.Add(resource);
            return this.Created(id.ToString(), id);
        }

        /// <summary>
        /// Updates resource value.
        /// </summary>
        [HttpPatch]
        [Route("{id}/{value}")]
        public async Task<IActionResult> Update(int id, string value)
        {
            var response = await this.resourcesAdapter.Update(id, value);
            if (response < 1)
            {
                return this.BadRequest("Invalid data.");
            }

            return Ok(response.ToString());
        }

        /// <summary>
        /// Updates resource target binding.
        /// </summary>
        [HttpPut]
        [Route("bind/{id}/{value}")]
        public async Task<IActionResult> Bind(int id, int targetId)
        {
            var response = await this.resourcesAdapter.UpdateBinding(id, targetId);
            if (response < 1)
            {
                return this.BadRequest("Invalid data.");
            }

            return Ok(response.ToString());
        }
    }
}