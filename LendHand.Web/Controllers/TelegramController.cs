﻿using LendHand.AbstractWeb.Extensions;
using LendHand.Telegram.Business.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace LendHand.Web.Controllers
{
    /// <summary>
    /// Controller process LendHand telegram bot updates.
    /// </summary>
    [Route("api/[controller]")]
    public class TelegramController : ControllerBase
    {
        private readonly ITelegramProcessor processor;

        /// <summary>
        /// Creates new instance of <see cref="TelegramController"/>.
        /// </summary>
        public TelegramController(ITelegramProcessor processor)
        {
            this.processor = processor;
        }

        /// <summary>
        /// Telegram bot onUpdate event handling.
        /// </summary>
        /// <returns>Action.</returns>
        [HttpPost]
        public async Task<IActionResult> OnUpdate()
        {
            var update = await this.Parse<Update>();
            await processor.Update(update);
            return this.Ok();
        }
    }
}