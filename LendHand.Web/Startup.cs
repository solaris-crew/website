using AutoMapper;
using LendHand.AbstractWeb.Extensions;
using LendHand.AbstractWeb.Models;
using LendHand.AbstractWeb.StartupConfiguration;
using LendHand.Common.Repository;
using LendHand.Resources.Adapter;
using LendHand.Resources.Data;
using LendHand.Resources.Data.Entities;
using LendHand.Resources.Data.Repository;
using LendHand.Resources.Data.Repository.Interfaces;
using LendHand.State;
using LendHand.State.Services;
using LendHand.Telegram.Business.Abstraction;
using LendHand.Telegram.Business.BotClient;
using LendHand.Telegram.Business.Commands;
using LendHand.Telegram.Business.Commands.Extensions;
using LendHand.Telegram.Business.Commands.Organization;
using LendHand.Telegram.Business.Mapping;
using LendHand.Telegram.Business.Services;
using LendHand.Telegram.Business.Services.Interfaces;
using LendHand.Telegram.Data;
using LendHand.Telegram.Data.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Reflection;
using static LendHand.Web.System.Constants;

namespace LendHand.Telegram.Web
{
    /// <summary>
    /// Crm main service startup configuration class.
    /// </summary>
    public class Startup : LendHandStartupBase
    {
        /// <summary>
        /// Creates new instance of main crm startup using <see cref="IConfiguration"/> object.
        /// </summary>
        /// <param name="configuration">Application configuration.</param>
        public Startup(IConfiguration configuration) : base(configuration) { }

        /// <summary>
        /// Configure Host services and DI.
        /// </summary>
        /// <param name="services">Collection to configure.</param>
        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.ConfigureSwagger(new SwaggerSettingsDto
            {
                Title = "LendHand API",
                XmlAssemblyName = Assembly.GetExecutingAssembly().GetName().Name,
                XmlBasePath = AppContext.BaseDirectory
            });
            AddTelegramBotSettings(services);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var opt = app.ApplicationServices.GetService<IOptions<TelegramBotSettings>>();
            TelegramBot.GetBotClientAsync(opt.Value)
                .Wait();
            base.Configure(app, env);
        }

        /// <summary>
        /// Add services and cases.
        /// </summary>
        protected override void AddServices(IServiceCollection services)
        {
            services.AddTransient<IResourcesAdapter, ResourcesAdapter>();
            services.AddTransient<ILanguageService, LanguageService>();
            services.AddTransient<IVolunteerService, VolunteerService>();
            services.AddTransient<INeedyService, NeedyService>();

            services.AddTransient<ITelegramProcessor, TelegramProcessor>();

            services.AddTransient<INeedyService, NeedyService>();
            services.AddTransient<IVolunteerService, VolunteerService>();
            services.AddTransient<IStateService, StateService>();

            services.AddTransient<ICommandProvider, TelegramCommandProvider>();
            services.AddTransient<IBehaviorProvider, BehaviorProvider>();

            services.AddTransient<StartCommand>();
            services.AddTransient<ErrorCommand>();
            services.AddTransient<RestartAllCommand>();
            services.AddTransient<IgnoreCommand>();

            services.AddTransient<SignInOrgCommand>();
            services.AddTransient<SignInOrgCancelCommand>();
            services.AddTransient<SignInOrgCountryCommand>();
            services.AddTransient<SignInOrgNameCommand>();

            services.AddTransient<VolunteerCommand>();
            services.AddTransient<NeedyCommand>();
        }

        /// <summary>
        /// Add mapping profiles.
        /// </summary>
        protected override void AddMapping(IServiceCollection services)
        {
            services.AddAutoMapper(x =>
            {
                x.AddProfile<ResourceMappingProfile>();
                x.AddProfile<BotProfile>();
            }, typeof(Startup));
        }

        /// <summary>
        /// Add database.
        /// </summary>
        protected override void AddDatabase(IServiceCollection services)
        {
            services.AddDbContext<TelegramDbContext>(options =>
            {
                options.UseSqlite(
                    Configuration.GetConnectionString(SETTING_NAME_DB0),
                        x => x.MigrationsAssembly(DATA_ASSEMBLY_NAME));
            }, ServiceLifetime.Scoped);

            services.AddDbContext<ResourcesDbContext>(options =>
            {
                options.UseSqlite(
                    Configuration.GetConnectionString(RESOURCES_NAME_DB1),
                        x => x.MigrationsAssembly(RESOURCE_DATA_ASSEMBLY_NAME));
            }, ServiceLifetime.Scoped);

            services.AddDbContext<StateContext>(options =>
            {
                options.UseSqlite(
                    Configuration.GetConnectionString(STATE_NAME_DB2),
                        x => x.MigrationsAssembly(STATE_DATA_ASSEMBLY_NAME));
            }, ServiceLifetime.Scoped);

            services.AddScoped<IRepository<Resource>, ResouresRepository>();
            services.AddScoped<IRepository<State.Entities.State>, StateRepository>();
            services.AddScoped<ILanguageRepository, LanguageRepository>();
            services.AddScoped<IVolunteerRepository, VolunteerRepository>();
            services.AddScoped<INeedyRepository, NeedyRepository>();
        }

        /// <summary>
        /// Add telegram bot settings
        /// </summary>
        /// <param name="services"></param>
        protected virtual void AddTelegramBotSettings(IServiceCollection services)
        {
            services.Configure<TelegramBotSettings>(
                this.Configuration.GetSection("TelegramBotSettings"));
        }
    }
}
