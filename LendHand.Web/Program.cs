using LendHand.AbstractWeb;
using LendHand.AbstractWeb.Extensions;
using LendHand.Resources.Data;
using LendHand.State;
using LendHand.Telegram.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace LendHand.Telegram.Web
{
    /// <summary>
    /// Endpoint class.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// App Endpoint method.
        /// </summary>
        /// <param name="args">Terminal arguments.</param>
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .CreateLogger();

            var configurator = new HostBuilderConfigurator();
            var builder = configurator.Configure<Startup>(args);
            builder.ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseIISIntegration();
            });
            var host = builder.Build();
            host.MigrateDatabase<TelegramDbContext>();
            host.MigrateDatabase<ResourcesDbContext>();
            host.MigrateDatabase<StateContext>();
            host.Run();
        }
    }
}
