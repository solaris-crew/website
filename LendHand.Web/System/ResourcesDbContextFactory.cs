﻿using LendHand.Resources.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;
using static LendHand.Web.System.Constants;

namespace LendHand.Web.System
{
    /// <summary>
    /// Factory which allows create db context instance for migration.
    /// </summary>
    public class ResourcesDbContextFactory : IDesignTimeDbContextFactory<ResourcesDbContext>
    {

        /// <summary>
        /// Creates new instance of <see cref="ResourcesDbContext"/>.
        /// </summary>
        public ResourcesDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ResourcesDbContext>();
            var configuration = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json")
               .Build();

            var connectionString = configuration.GetConnectionString(RESOURCES_NAME_DB1);
            optionsBuilder.UseSqlite(connectionString);

            return new ResourcesDbContext(optionsBuilder.Options);
        }
    }
}
