﻿using LendHand.Resources.Data;
using LendHand.State;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;
using static LendHand.Web.System.Constants;

namespace LendHand.Web.System
{
    public class StateDbFactory : IDesignTimeDbContextFactory<StateContext>
    {

        /// <summary>
        /// Creates new instance of <see cref="ResourcesDbContext"/>.
        /// </summary>
        public StateContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<StateContext>();
            var configuration = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json")
               .Build();

            var connectionString = configuration.GetConnectionString(STATE_NAME_DB2);
            optionsBuilder.UseSqlite(connectionString);

            return new StateContext(optionsBuilder.Options);
        }
    }
}
