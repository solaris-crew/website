﻿namespace LendHand.Web.System
{
    /// <summary>
    /// Startup consts for web app.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Default sql connection string.
        /// </summary>
        public static readonly string SETTING_NAME_DB0 = nameof(Contracts.Settings.ConnectionStrings.Db0);
        /// <summary>
        /// Resources database connections string.
        /// </summary>
        public static readonly string RESOURCES_NAME_DB1 = nameof(Contracts.Settings.ConnectionStrings.Db1);
        /// <summary>
        /// State database connections string.
        /// </summary>
        public static readonly string STATE_NAME_DB2 = nameof(Contracts.Settings.ConnectionStrings.Db2);
        /// <summary>
        /// Assembly which contains dbcontext name.
        /// </summary>
        public static readonly string DATA_ASSEMBLY_NAME = "LendHand.Telegram.Data";
        /// <summary>
        /// Assembly which contains resource dbcontext name.
        /// </summary>
        public static readonly string RESOURCE_DATA_ASSEMBLY_NAME = "LendHand.Resources.Data";
        /// <summary>
        /// Assembly which contains resource dbcontext name.
        /// </summary>
        public static readonly string STATE_DATA_ASSEMBLY_NAME = "LendHand.State";
    }
}
