﻿using LendHand.Telegram.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;
using static LendHand.Web.System.Constants;

namespace LendHand.Telegram.Web.System
{
    /// <summary>
    /// Factory which allows create db context instance for migration.
    /// </summary>
    public class TGDBContextDesignFactory : IDesignTimeDbContextFactory<TelegramDbContext>
    {
        /// <summary>
        /// Creates new instance of <see cref="TelegramDbContext"/>.
        /// </summary>
        public TelegramDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<TelegramDbContext>();
            var configuration = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile("appsettings.json")
               .Build();

            var connectionString = configuration.GetConnectionString(SETTING_NAME_DB0);
            optionsBuilder.UseSqlite(connectionString);

            return new TelegramDbContext(optionsBuilder.Options);
        }
    }
}
